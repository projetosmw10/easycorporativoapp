import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { ApiProvider } from '../../providers/api/api';
import { TabsProvider } from '../../providers/tabs/tabs';
import { GestaoCadastroPage } from '../gestao-cadastro/gestao-cadastro';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasConteudosPage } from '../categorias-conteudos/categorias-conteudos';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { AreaProvider } from '../../providers/area/area';
import { ConsultasProvider } from '../../providers/consultas/consultas';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gestao-imagens',
  templateUrl: 'gestao-imagens.html'
})
export class GestaoImagensPage {

  @ViewChild(Refresher) refresher: Refresher;
  area_id = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _area: AreaProvider,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider,
    public _consultas: ConsultasProvider,
    public helpers: HelpersProvider,
    public socialSharing: SocialSharing
  ) {
    
    this.onRefresher();
  }

  onRefresher() {
    
    this._consultas.getImagens().then(() => {
      this.loading.dismiss()
      this.refresher.complete()
      console.log(this._consultas.list)
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })

    
  }

  ionViewDidEnter() {
    this._area.getAll().then(r => {
      this.loading.dismiss();
      this.refresher.complete()
    }, e => {
      this.loading.dismiss();
      this.refresher.complete()
    })
  }

  doSearch(e) {
  
    this._consultas.getImagens(this.area_id).then(() => {
      this.loading.dismiss()
      this.refresher.complete()
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })
  }

  shareImg(item) {
    

      this.socialSharing.share(null, null, item.thumbnail_principal, null)
      .then(() => {
      
      },
      () => {
      
      })
     
  }
}
