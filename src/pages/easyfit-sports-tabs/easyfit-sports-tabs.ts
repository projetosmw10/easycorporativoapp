import { Component, ViewChild } from '@angular/core';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';


import { FavoritosPage } from '../favoritos/favoritos';
import { NavParams, Tabs } from 'ionic-angular';
import { ContatoPage } from '../contato/contato';
import { CategoriasConteudosPage } from '../categorias-conteudos/categorias-conteudos';
import { PdfsEDicasPage } from '../pdfs-e-dicas/pdfs-e-dicas';
import { PdfsProvider } from '../../providers/pdfs/pdfs';
import { EasyfitSportsHomePage } from '../easyfit-sports-home/easyfit-sports-home';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';

@Component({
  templateUrl: 'easyfit-sports-tabs.html'
})
export class EasyfitSportsTabsPage {
  @ViewChild('tabRef') tabRef: Tabs;

  tab0Root = CategoriasConteudosPage;
  tab1Root = FavoritosPage;
  tab2Root = EasyfitSportsHomePage;
  tab3Root = PdfsEDicasPage;
  tab4Root = ContatoPage;
  loaded: boolean = false;
  tabIndex: number = 0;

  select;
  constructor(
    public navParams: NavParams,
    public nativePageTransitions: NativePageTransitions,
    public _pdfs: PdfsProvider,
    public aplicativos: AplicativosProvider
  ) {
    this.select = this.navParams.get('select');
    this._pdfs.getAllIfNecessary();
    console.log(aplicativos);
    console.log('item', aplicativos.item)
  }
  ionViewDidLoad() {
    if (this.select !== undefined) {
      this.tabRef.select(this.select);
    }
  }

  getAnimationDirection(index) {
    var currentIndex = this.tabIndex;

    this.tabIndex = index;

    switch (true) {
      case (currentIndex < index):
        return ('left');
      case (currentIndex > index):
        return ('right');
    }
  }

  transition(e) {
    let options: NativeTransitionOptions = {
      direction: this.getAnimationDirection(e.index),
      duration: 300,
      slowdownfactor: -.5,
      slidePixels: 0,
      iosdelay: 10,
      androiddelay: 10,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0,


    };

    if (!this.loaded) {
      this.loaded = true;
      return;
    }

    this.nativePageTransitions.slide(options);
  }


}
