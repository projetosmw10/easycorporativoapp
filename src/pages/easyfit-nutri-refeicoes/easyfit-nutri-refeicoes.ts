import { EasyfitNutriRefeicoesInternaPage } from './../easyfit-nutri-refeicoes-interna/easyfit-nutri-refeicoes-interna';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, InfiniteScroll } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Subscription } from 'rxjs/Subscription';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';

/**
 * Generated class for the RefeicoesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-easyfit-nutri-refeicoes',
  templateUrl: 'easyfit-nutri-refeicoes.html',
  
})
export class EasyfitNutriRefeicoesPage {
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  refeicoes: any = new Array(6);
  tipo = 0;

  options = {
    favoritado: 0,
    search: '',
    page: 1
  }

  loading = false;
  apiSearch: Subscription;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public _alimentos: EasyfitNutriAlimentosProvider
  ) {
    
    this._alimentos.getDestaques().subscribe();
    this.doSearch();
  }
  ionViewWillEnter () {
    this.tipo = this._alimentos.tipo;
    this.tipo = this.tipo ? this.tipo : 1;
  }

  refeicaoInternaPage(item) {
    this.navCtrl.push(EasyfitNutriRefeicoesInternaPage, { tipo: this.tipo, item: item });
  }

  doSearch() {
    this.loading = true;
    this.options.page = 1;
    this._alimentos._getPaginate = this._alimentos.getPaginate(this.options).subscribe(r => {
      this.loading = false;
    }, e => this.loading = false
    )
  }

  doInfinite() {
    this.options.page++;
    this._alimentos._getPaginate = this._alimentos.getPaginate(this.options).subscribe(r => {
      this.infiniteScroll.complete();
      if (r.data.length === 0) {
        this.options.page--;
      }
    }, e => this.infiniteScroll.complete());
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad RefeicoesPage');
  }

  temDestaqueFavoritado() {
    return this._alimentos.destaques.find(obj => obj.favoritado)
  }
}
