import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { AreaProvider } from '../../providers/area/area';
import { ApiProvider } from '../../providers/api/api';
import { TabsProvider } from '../../providers/tabs/tabs';
import { ToastController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { NgForm } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ConsultasProvider } from '../../providers/consultas/consultas';
import { CentralNoticiasPage } from '../central-noticias/central-noticias';
import { HttpProgressEvent } from '@angular/common/http';
import { HelpersProvider } from '../../providers/helpers/helpers';
/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-central-noticias-interna',
  templateUrl: 'central-noticias-interna.html'
})
export class CentralNoticiasInternaPage {
  subareas: any = [];
  noticia: any = {
    titulo : '', 
    data : '', 
    texto: '', 
    thumbnail_principal: ''
  };

  parentPage : CentralNoticiasPage;

  @ViewChild(Refresher) refresher: Refresher;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _areas: AreaProvider,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider,
    public toastCtrl: ToastController,
    public auth: AuthProvider,
    public helpers: HelpersProvider
  ) {
    if (!this._areas.list.length) {
      this.loading.present();
    }
    this.onRefresher();

    if (this.navParams.get('item'))
    {
      this.noticia = this.navParams.get('item');
      }

    this.parentPage = this.navParams.get('parentPage');
  }

  onRefresher() {

    this.ionViewDidEnter();
  }

  ionViewDidEnter() {
   
  }


}
