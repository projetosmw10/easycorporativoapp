import { CadastroUsuarioSecondPage } from './../cadastro-usuario-second/cadastro-usuario-second';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ToastController, Content, App } from 'ionic-angular';
import { CadastroProvider } from '../../providers/cadastro/cadastro';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { EasyfitSportsTabsPage } from '../easyfit-sports-tabs/easyfit-sports-tabs';
import { LoadingProvider } from '../../providers/loading/loading';

/**
 * Generated class for the CadastroUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cadastro-usuario',
  templateUrl: 'cadastro-usuario.html',
})
export class CadastroUsuarioPage {
  heightFruits = 0;
  @ViewChild('dataNascimento') dataNascimento: ElementRef;
  @ViewChild(Content) content: Content;
  @ViewChild('layout') layout: ElementRef
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _cadastro: CadastroProvider,
    public helpers: HelpersProvider,
    public toastCtrl: ToastController,
    public aplicativos: AplicativosProvider,
    public app: App,
    public loading: LoadingProvider
  ) {
  }

  ionViewDidLoad() {
    this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
    if (this.heightFruits < 150) {
      this.heightFruits = 150;
    }
  }

  dataNascimentoPicker() {
    this.dataNascimento.nativeElement.click();
  }

  cadastroUsuarioSecondPage() {
    if (!this._cadastro.dados.genero) {
      this.toastCtrl.create({ message: 'Qual o seu gênero?', duration: 3000 }).present();
      return;
    }
    if (!this._cadastro.dados.data_nascimento) {
      this.toastCtrl.create({ message: 'Qual a sua data de nascimento?', duration: 3000 }).present();
      return;
    }
    if (!this._cadastro.dados.altura || !(this._cadastro.dados.altura < 300 && this._cadastro.dados.altura > 0)) {
      this.toastCtrl.create({ message: 'Qual a sua altura?', duration: 3000 }).present();
      return;
    }
    if (!this._cadastro.dados.inicio_kg || !(this._cadastro.dados.inicio_kg < 300 && this._cadastro.dados.inicio_kg > 0)) {
      this.toastCtrl.create({ message: 'Qual o seu peso?', duration: 3000 }).present();
      return;
    }
    this._cadastro.dados.atual_kg = this._cadastro.dados.inicio_kg;
    this.navCtrl.push(CadastroUsuarioSecondPage, {});
  }


  easylifePage() {
    this.loading.present()
    this.app.getRootNav().setRoot(EasyfitSportsTabsPage, { animate: true, animation: 'ios-transition', duration: 1000 });
    setTimeout(() => {
      this.aplicativos.setItem(this.aplicativos.getEasyfitLife())
      this.loading.dismiss()
    }, 50)
  }
}
