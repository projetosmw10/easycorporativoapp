import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { TabsProvider } from '../../providers/tabs/tabs';
import { LoadingProvider } from '../../providers/loading/loading';

/**
 * Generated class for the CategoriasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {

  @ViewChild(Refresher) refresher: Refresher;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public tabs: TabsProvider,
    public _categorias: CategoriasProvider,
    public loading: LoadingProvider
  ) {
    if (this._categorias.list.length == 0) {
      this.loading.present();
      this._categorias.getAll()
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriasPage');
  }

  ionViewDidEnter() {
    this._categorias.getAll().then(() => {
      this.loading.dismiss()
      this.refresher.complete()
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })
  }

  onRefresher() {
    this.ionViewDidEnter();
  }

  openRoteiro(index) {
    this._categorias.closeAll();
    this.tabs.selectTab(0);
    setTimeout(() => this._categorias.watchToggle.next(index), 400);
  }

}
