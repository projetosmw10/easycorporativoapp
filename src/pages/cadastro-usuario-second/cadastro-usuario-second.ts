import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ToastController, Content } from 'ionic-angular';
// import { CadastroUsuarioThirdPage } from './../cadastro-usuario-third/cadastro-usuario-third';
import { CadastroProvider } from '../../providers/cadastro/cadastro';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { ApiProvider } from '../../providers/api/api';
import { AuthProvider } from '../../providers/auth/auth';
import { LoadingProvider } from '../../providers/loading/loading';
import { EasyfitNutriTabsPage } from '../easyfit-nutri-tabs/easyfit-nutri-tabs';

/**
 * Generated class for the CadastroUsuarioSecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cadastro-usuario-second',
  templateUrl: 'cadastro-usuario-second.html',
})
export class CadastroUsuarioSecondPage {

  heightFruits = 0;
  @ViewChild(Content) content: Content;
  @ViewChild('layout') layout: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _cadastro: CadastroProvider,
    public helpers: HelpersProvider,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public auth: AuthProvider,
    public loading: LoadingProvider
  ) {
  }

  ionViewDidLoad() {
    this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
    if(this.heightFruits < 150) {
      this.heightFruits = 150;
    }
  }

  cadastroUsuarioThirdPage() {
    /** @todo descomentar validacao */
    if (!this._cadastro.dados.objetivo_descricao) {
      this.toastCtrl.create({ message: 'Qual o seu objetivo?', duration: 3000 }).present();
      return;
    }

    if (!this._cadastro.dados.objetivo_kg || !(this._cadastro.dados.objetivo_kg < 300 && this._cadastro.dados.objetivo_kg > 0)) {
      this.toastCtrl.create({ message: 'Qual o seu objetivo (kg)?', duration: 3000 }).present();
      return;
    }

    if (!this._cadastro.dados.objetivo_kcal || !(this._cadastro.dados.objetivo_kcal < 10000 && this._cadastro.dados.objetivo_kcal > 0)) {
      this.toastCtrl.create({ message: 'Qual o seu objetivo (kcal/dia)?', duration: 3000 }).present();
      return;
    }
    delete this._cadastro.dados.password;
    delete this._cadastro.dados.first_name;
    this.api.post('/users/' + this.auth.user.user.id, this._cadastro.dados).subscribe(
      r => {
        this.toastCtrl.create({ message: 'Dados salvos com sucesso!', duration: 4000 }).present();
        this.loading.dismiss();
        this.auth.setToken(r.token);
        this._cadastro.dados = this.auth.user.user;
        this.navCtrl.setRoot(EasyfitNutriTabsPage)
      },
      e => this.loading.dismiss()
    )
  }

}
