import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { ApiProvider } from '../../providers/api/api';
import { TabsProvider } from '../../providers/tabs/tabs';
import { GestaoCadastroPage } from '../gestao-cadastro/gestao-cadastro';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasConteudosPage } from '../categorias-conteudos/categorias-conteudos';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { AreaProvider } from '../../providers/area/area';
import { NotificacoesProvider } from '../../providers/notificacoes/notificacoes';
import { HelpersProvider } from '../../providers/helpers/helpers';
/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-central-notificacoes',
  templateUrl: 'central-notificacoes.html'
})
export class CentralNotificacoesPage {

  @ViewChild(Refresher) refresher: Refresher;
  area_id = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider,
    public _notificacoes: NotificacoesProvider,
    public helpers: HelpersProvider
  ) {
    if (!this._notificacoes.list.length) {
      this.loading.present();
    }
    this.onRefresher();
  }

  onRefresher() {   

    this._notificacoes.getAll().then(() => {
      this.loading.dismiss()
      this.refresher.complete()
      
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })
  }


  ionViewDidEnter() {
    this._notificacoes.getAll().then(r => {
      this.loading.dismiss();
      this.refresher.complete()
    }, e => {
      this.loading.dismiss();
      this.refresher.complete()
    })
  }

}
