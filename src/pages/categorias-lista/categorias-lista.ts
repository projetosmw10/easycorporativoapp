import { Component, ViewChild, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { NavController, NavParams, Refresher, Content } from 'ionic-angular';
import { PlayerPage } from '../player/player';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasProvider } from '../../providers/categorias/categorias';

/**
 * Generated class for the CategoriasListaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-categorias-lista',
  templateUrl: 'categorias-lista.html',
})
export class CategoriasListaPage {
  @ViewChild(Refresher) refresher: Refresher;
  @ViewChild(Content) content: Content;
  @ViewChildren('itemsRef') itemsRef: QueryList<ElementRef>;
  conteudos = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _categorias: CategoriasProvider,
    public loading: LoadingProvider
  ) {
    if (this._categorias.list.length == 0) {
      this.loading.present();
      this.onRefresher();
    }
    this._categorias.watchToggle.subscribe(index => this.toggleItem(index));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriasListaPage');
  }

  onRefresher() {
    this._categorias.getAll().then(() => {
      this.loading.dismiss()
      this.refresher.complete()
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })
  }

  playerPage(item) {
    this.navCtrl.push(PlayerPage, { item: item });
  }

  toggleItem(i) {
    this._categorias.toggleItem(i);
    this.conteudos = this._categorias.list[i].topicos;
    this.content.scrollToTop(this.itemsRef.toArray()[i].nativeElement.offsetTop);
    this.content.resize();
  }

}
