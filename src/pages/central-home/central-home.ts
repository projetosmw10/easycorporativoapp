import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { ApiProvider } from '../../providers/api/api';
import { TabsProvider } from '../../providers/tabs/tabs';
import { GestaoCadastroPage } from '../gestao-cadastro/gestao-cadastro';
import { GestaoListagemPage } from '../gestao-listagem/gestao-listagem';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasConteudosPage } from '../categorias-conteudos/categorias-conteudos';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { AreaProvider } from '../../providers/area/area';
import { CentralNotificacoesPage } from '../central-notificacoes/central-notificacoes';
import { CentralNoticiasPage } from '../central-noticias/central-noticias';

/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-central-home',
  templateUrl: 'central-home.html'
})
export class CentralHomePage {

  @ViewChild(Refresher) refresher: Refresher;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _area: AreaProvider,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider
  ) {
    if (!this._area.list.length) {
      this.loading.present();
    }
    this.onRefresher();
  }

  onRefresher() {
    console.log(this._area.getAll());
    this._area.getAll().then(() => {
      this.loading.dismiss()
      this.refresher.complete()
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })
  }

  ionViewDidEnter() {
    this._area.getAll().then(r => {
      this.loading.dismiss();
      this.refresher.complete()
    }, e => {
      this.loading.dismiss();
      this.refresher.complete()
    })
  }

  categoriasConteudosPage(item) {
    this._area.current = item;
    this.navCtrl.push(CategoriasConteudosPage);
  }

  noticias() {
    this.navCtrl.push(CentralNoticiasPage)
  }

  notificacoes() {
    this.navCtrl.push(CentralNotificacoesPage);
  }


}
