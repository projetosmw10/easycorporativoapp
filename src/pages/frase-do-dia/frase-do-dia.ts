import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the FraseDoDiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-frase-do-dia',
  templateUrl: 'frase-do-dia.html',
})
export class FraseDoDiaPage {
  frase = {};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider) {
    this.api.get('/frases').subscribe(r => {
      this.frase = r;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FraseDoDiaPage');
  }
  ionViewWillEnter() {
    this.api.get('/frases').subscribe(r => {
      this.frase = r;
    })
  }

}
