import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { AreaProvider } from '../../providers/area/area';
import { ApiProvider } from '../../providers/api/api';
import { TabsProvider } from '../../providers/tabs/tabs';
import { ToastController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { NgForm } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ConsultasProvider } from '../../providers/consultas/consultas';
import { GestaoListagemPage } from '../gestao-listagem/gestao-listagem';
import { HttpProgressEvent } from '@angular/common/http';
/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gestao-cadastro',
  templateUrl: 'gestao-cadastro.html'
})
export class GestaoCadastroPage {
  subareas: any = [];
  consulta: any = {
    area_id : '', 
    subarea_id : '', 
    nome: '', 
    telefone: '', 
    data_consulta: '', 
    observacoes: '', 
    tem_revisao: false, 
    data_revisao: '',
    thumbnail_principal: ''
  };

  file: File = null;
  imgSrc: any = '';

  minDate: string = new Date().toISOString();

  parentPage : GestaoListagemPage;

  @ViewChild(Refresher) refresher: Refresher;
  @ViewChild('file') inputFile: ElementRef;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _areas: AreaProvider,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider,
    public toastCtrl: ToastController,
    public auth: AuthProvider,
  ) {
    if (!this._areas.list.length) {
      this.loading.present();
    }
    this.onRefresher();

    if (this.navParams.get('item'))
    {
      this.consulta = this.navParams.get('item');
      this.imgSrc = this.consulta.thumbnail_principal ? this.consulta.thumbnail_principal : '';
      console.log(this.imgSrc);
      this.doSearch(this.consulta.area_id);
    }

    this.parentPage = this.navParams.get('parentPage');
  }

  onRefresher() {

    this._areas.getAll().then(() => {
      this.loading.dismiss()
      this.refresher.complete()
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })

    this.ionViewDidEnter();
  }

  ionViewDidEnter() {
    this._areas.getAll().then(r => {
      this.loading.dismiss();
      this.refresher.complete()
    }, e => {
      this.loading.dismiss();
      this.refresher.complete()
    })
  }


  doSearch(e) {
    let area = this._areas.list.find(area => area.id == e);
    this.subareas = area.subareas;
  }

  onSubmit(form: NgForm) {
   
   
    if (!form.valid) {
      this.toastCtrl.create({ message: 'Verifique se todos os campos estão preenchidos', duration: 4000 }).present();
      return
    }
    this.loading.present();
    this.api.post('/users/' + this.auth.user.user.id + '/consultas', this.consulta).subscribe(r => {
      this.loading.dismiss()
      this.toastCtrl.create({ message: 'Sua consulta foi cadastrada com sucesso!', duration: 4000 }).present();
      this.consulta = {
        area_id : '', 
        subarea_id : '', 
        nome: '', 
        telefone: '', 
        data_consulta: '', 
        observacoes: '', 
        tem_revisao: '', 
        data_revisao: '',
        thumbnail_principal: ''
      }

      this.imgSrc = '';
      this.navCtrl.push(GestaoListagemPage);
    }, e => this.loading.dismiss())
  }

  onChangeInputFiles(files: FileList) {
    this.file = files.item(0);
    this.submitImagens('/users/' + this.auth.user.user.id +'/imagemConsulta').then((e: HttpProgressEvent | any)=> {
      this.consulta.thumbnail_principal = e.body.thumbnail_principal;
      this.imgSrc = e.body.preview;;
    });
  }

  submitImagens(url) {
    return new Promise(resolve => {
      
      this.loading.present();
      this.api.postFile(this.file, url)
        .subscribe((event: HttpProgressEvent | any) => {
          if (event.type === 4) {
            this.loading.dismiss();
            resolve(event);
          }
        },
          err => this.loading.dismiss()
        );
    })
  }


}
