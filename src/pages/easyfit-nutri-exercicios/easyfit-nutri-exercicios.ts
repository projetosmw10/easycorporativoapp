import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, InfiniteScroll } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { ApiProvider } from '../../providers/api/api';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { EasyfitNutriExerciciosProvider } from '../../providers/easyfit-nutri-exercicios/easyfit-nutri-exercicios';
import { EasyfitNutriExercicioInternaPage } from '../easyfit-nutri-exercicio-interna/easyfit-nutri-exercicio-interna';
import { HelpersProvider } from '../../providers/helpers/helpers';

/**
 * Generated class for the ExerciciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-easyfit-nutri-exercicios',
  templateUrl: 'easyfit-nutri-exercicios.html',
})
export class EasyfitNutriExerciciosPage {
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  options = {
    search: '',
    page: 1
  }

  loading = false;
  apiSearch: Subscription;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public _alimentos: EasyfitNutriAlimentosProvider,
    public _exercicios: EasyfitNutriExerciciosProvider,
    public helpers: HelpersProvider
  ) {

    this._exercicios.getDay();
    this.doSearch();
  }
  ionViewWillEnter() {

  }

  exercicioInternaPage(item, atualiza?) {
    const quantidade = item.pivot ? item.pivot.quantidade : null;
    this.navCtrl.push(EasyfitNutriExercicioInternaPage, { item: item, atualiza: atualiza, quantidade: quantidade });
  }

  doSearch() {
    this.loading = true;
    this.options.page = 1;
    this._exercicios._getPaginate = this._exercicios.getPaginate(this.options).subscribe(r => {
      this.loading = false;
    }, e => this.loading = false
    )
  }

  doInfinite() {
    this.options.page++;
    this._exercicios._getPaginate = this._exercicios.getPaginate(this.options).subscribe(r => {
      this.infiniteScroll.complete();
      if (r.data.length === 0) {
        this.options.page--;
      }
    }, e => this.infiniteScroll.complete());
  }


}
