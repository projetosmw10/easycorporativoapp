import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, Content, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { ConteudosProvider } from '../../providers/conteudos/conteudos';
import { LoadingProvider } from '../../providers/loading/loading';
import { PlayerPage } from '../player/player';
import { CodigoPage } from '../codigo/codigo';
import { GlobalProvider } from '../../providers/global/global';
import { CertificadosPagarPage } from '../certificados-pagar/certificados-pagar';

/**
 * Generated class for the ModuloPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-categorias-conteudos',
  templateUrl: 'categorias-conteudos.html',
})
export class CategoriasConteudosPage {
  @ViewChild(Refresher) refresher: Refresher;
  @ViewChild(Content) content: Content;

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private _categorias: CategoriasProvider,
    private loading: LoadingProvider,
    public global: GlobalProvider
  ) {
  }

  ionViewDidLoad(e) {

    if (!this._categorias.current.id) {
      this._categorias.current = this._categorias.list[0];
    }
    this.loading.present();
    this._categorias.getTopicos(this._categorias.current.id).then(r => {
      this._categorias.current.topicos = r;
      this._categorias.current = this._categorias.parseCategoria(this._categorias.current);
      this.refresher.complete();
      this.loading.dismiss();
    }).catch(r => this.loading.dismiss());

  }

  ionViewWillEnter() {
    console.log('will enter')
    if(this._categorias.current.topicos && this._categorias.current.topicos.length &&
      this._categorias.current.topicos.topicos) {
      this._categorias.current = this._categorias.parseCategoria(this._categorias.current);
    }
  }

  onRefresher() {
    return new Promise((resolve, reject) => {
      this._categorias.getTopicos(this._categorias.current.id).then(r => {
        this._categorias.current.topicos = r;
        this._categorias.current = this._categorias.parseCategoria(this._categorias.current);
        this.refresher.complete();
        this.loading.dismiss();
        resolve()
      }, e => {
        this.refresher.complete();
        this.loading.dismiss();
        reject()
      })
    })
  }


  certificadosPagarPage() {
    this.navCtrl.push(CertificadosPagarPage, {categoria: Object.assign({}, this._categorias.current)})
  }
}
