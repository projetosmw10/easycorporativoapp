import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { GlobalProvider } from '../../providers/global/global';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { AuthProvider } from '../../providers/auth/auth';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { Subscription } from 'rxjs/Subscription';
import { Alimento } from '../../models/alimento';
import { EasyfitNutriRefeicoesPage } from '../easyfit-nutri-refeicoes/easyfit-nutri-refeicoes';
import { EasyfitNutriRefeicoesInternaPage } from '../easyfit-nutri-refeicoes-interna/easyfit-nutri-refeicoes-interna';
import { EasyfitNutriAguaProvider } from '../../providers/easyfit-nutri-agua/easyfit-nutri-agua';
import { EasyfitNutriExerciciosProvider } from '../../providers/easyfit-nutri-exercicios/easyfit-nutri-exercicios';
import { EasyfitNutriControleAguaPage } from '../easyfit-nutri-controle-agua/easyfit-nutri-controle-agua';

@Component({
  selector: 'page-easyfit-nutri-home',
  templateUrl: 'easyfit-nutri-home.html'
})
export class EasyfitNutriHomePage {
  
  alimentos = new Array<Alimento>();
  
  constructor(
    public navCtrl: NavController,
    public api: ApiProvider,
    public global: GlobalProvider,
    public helpers: HelpersProvider,
    public auth: AuthProvider,
    public _alimentos: EasyfitNutriAlimentosProvider,
    public _agua: EasyfitNutriAguaProvider,
    public _exercicios: EasyfitNutriExerciciosProvider
  ) {
    
    this._exercicios.getDay();
  }

  
  refeicoesPage(tipo) {
    this._alimentos.tipo = tipo;
    this.navCtrl.push(EasyfitNutriRefeicoesPage);
  }
  refeicoesInternaPage(item, tipo) {
    this._alimentos.tipo = tipo;
    this.navCtrl.push(EasyfitNutriRefeicoesInternaPage, { item: item, atualizar: true, quantidade: item.pivot.quantidade });
  }
  controleAguaPage() {
    this.navCtrl.push(EasyfitNutriControleAguaPage);
  }

  ionViewWillEnter() {
    this._alimentos.getDay(false);
    this._agua.get().subscribe();
  }




}
