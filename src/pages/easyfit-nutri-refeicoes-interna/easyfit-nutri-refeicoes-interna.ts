import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, LoadingController, Content } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { AuthProvider } from '../../providers/auth/auth';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { LoadingProvider } from '../../providers/loading/loading';

/**
 * Generated class for the RefeicaoInternaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-easyfit-nutri-refeicoes-interna',
  templateUrl: 'easyfit-nutri-refeicoes-interna.html',
})
export class EasyfitNutriRefeicoesInternaPage {
  item: any = {}
  tipo = 1;
  gordura;
  energia;
  carboidratos;
  proteina;

  quantidade;

  atualizar;

  heightFruits = 0;
  @ViewChild(Content) content: Content;
  @ViewChild('layout') layout: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public auth: AuthProvider,
    public helpers: HelpersProvider,
    public _alimentos: EasyfitNutriAlimentosProvider,
    public toastCtrl: ToastController,
    public loading: LoadingProvider
  ) {
    this.item = this.navParams.get('item');


    this.atualizar = this.navParams.get('atualizar');
    
    this.quantidade = this.navParams.get('quantidade');

    this.gordura = this.item.gordura;
    this.energia = this.item.energia;
    this.carboidratos = this.item.carboidratos;
    this.proteina = this.item.proteina;


    if(this.atualizar) {
      this.calcular();
    }

  }
  ionViewWillEnter() {
    this.tipo = this._alimentos.tipo;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RefeicaoInternaPage');
    this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
    if(this.heightFruits < 150) {
      this.heightFruits = 150;
    }
  }

  salvar() {
    if (!this.quantidade || this.quantidade < 0) {
      this.toastCtrl.create({ message: 'Por favor, informe a quantidade desejada', duration: 4000 }).present();
      return;
    }
    this.loading.present();
    this.request().subscribe(r => {
      this.toastCtrl.create({ message: 'Alimento salvo para o dia ' + this.helpers.moment(this._alimentos.selectedDate, 'YYYY-MM-DD').format('LL') + ' para ' + this._alimentos.tipos[this.tipo], duration: 4000 }).present();
      this._alimentos.getDay().then(() => {
        this.navCtrl.pop();
        this.loading.dismiss();
      });
    })

  }

  favoritar() {
    this._alimentos.favoritar(this.item).then(alimento => this.item = alimento);
  }

  calcular() {
    this.energia = Math.ceil((this.item.energia * this.quantidade) / 100);
    this.gordura = ((this.item.gordura * this.quantidade) / 100).toFixed(2);
    this.carboidratos = ((this.item.carboidratos * this.quantidade) / 100).toFixed(2);
    this.proteina = ((this.item.proteina * this.quantidade) / 100).toFixed(2);
  }


  request() {
    if (this.atualizar) {
      return this.api.patch('/users/' + this.auth.user.user.id + '/alimentos', {
        alimento_id: this.item.id,
        data: this._alimentos.selectedDate,
        quantidade: this.quantidade,
        tipo: this.tipo
      })
    } else {
      return this.api.post('/users/' + this.auth.user.user.id + '/alimentos', {
        alimento_id: this.item.id,
        data: this._alimentos.selectedDate,
        quantidade: this.quantidade,
        tipo: this.tipo
      })
    }
  }

}
