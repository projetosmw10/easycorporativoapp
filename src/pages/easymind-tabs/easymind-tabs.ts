import { Component, ViewChild } from '@angular/core';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';


import { CategoriasPage } from '../categorias/categorias';
import { FraseDoDiaPage } from '../frase-do-dia/frase-do-dia';
import { FavoritosPage } from '../favoritos/favoritos';
import { AudiosPage } from '../audios/audios';
import { NavParams, Tabs } from 'ionic-angular';
import { PerfilPage } from '../perfil/perfil';
import { CodigoPage } from '../codigo/codigo';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ConhecaPage } from '../conheca/conheca';
import { ApiProvider } from '../../providers/api/api';
import { AuthProvider } from '../../providers/auth/auth';
import { LoadingProvider } from '../../providers/loading/loading';
import { TabsProvider } from '../../providers/tabs/tabs';
import { CategoriasListaPage } from '../categorias-lista/categorias-lista';
import { CategoriasProvider } from '../../providers/categorias/categorias';

@Component({
  templateUrl: 'easymind-tabs.html'
})
export class EasymindTabsPage {
  @ViewChild('tabRef') tabRef: Tabs;

  tab0Root = CategoriasListaPage;
  tab1Root = FraseDoDiaPage;
  tab2Root = CategoriasPage;
  tab3Root = FavoritosPage;
  tab4Root = AudiosPage;
  tab5Root = PerfilPage;
  tab6Root = CodigoPage;
  tab7Root = ConhecaPage;
  loaded: boolean = false;
  tabIndex: number = 0;

  select;
  constructor(
    public navParams: NavParams,
    public nativePageTransitions: NativePageTransitions,
    public splashScreen: SplashScreen,
    public api: ApiProvider,
    public auth: AuthProvider,
    public _categorias: CategoriasProvider,
    public loading: LoadingProvider,
    public tabs: TabsProvider
  ) {
    this.select = this.navParams.get('select');
  }
  ionViewDidLoad() {
    // salva onesignal
    this.api.saveOneSignal();
    // encontra todas conteudos
    this._categorias.getAll().then(() => this.loading.dismiss(), () => this.loading.dismiss());
    //atualiza token do usuario
    
    setTimeout(() => this.splashScreen.hide(), 600);
    this.tabs._selectTab.subscribe(select => {
      try {
        this.tabRef.select(select)
      } catch (e) {
        console.error('BUGOU MAS LOGO DESBUGAMOS')
        try {
          this.tabRef.select(select)
        } catch (e) {
          // deu erro mas deu certo vai entender
          console.log(' and the gambiarra has won once again')
        }
      }
    });

    if (this.select !== undefined) {
      this.tabRef.select(this.select);
    }
  }

  getAnimationDirection(index) {
    var currentIndex = this.tabIndex;

    this.tabIndex = index;

    switch (true) {
      case (currentIndex < index):
        return ('left');
      case (currentIndex > index):
        return ('right');
    }
  }

  transition(e) {
    let options: NativeTransitionOptions = {
      direction: this.getAnimationDirection(e.index),
      duration: 300,
      slowdownfactor: -.5,
      slidePixels: 0,
      iosdelay: 10,
      androiddelay: 10,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0,


    };

    if (!this.loaded) {
      this.loaded = true;
      return;
    }

    this.nativePageTransitions.slide(options);
  }


}
