import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { EasyfitNutriExerciciosProvider } from '../../providers/easyfit-nutri-exercicios/easyfit-nutri-exercicios';
import { EasyfitNutriAguaProvider } from '../../providers/easyfit-nutri-agua/easyfit-nutri-agua';

/**
 * Generated class for the CalculoCaloricoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-easyfit-nutri-calculo-calorico',
  templateUrl: 'easyfit-nutri-calculo-calorico.html',
})
export class EasyfitNutriCalculoCaloricoPage {

  heightFruits = 0;
  @ViewChild(Content) content: Content;
  @ViewChild('layout') layout: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _alimentos: EasyfitNutriAlimentosProvider,
    public helpers: HelpersProvider,
    public _exercicios: EasyfitNutriExerciciosProvider,
    public _agua: EasyfitNutriAguaProvider
  ) {
  }

  ionViewDidLoad() {
    this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
    if(this.heightFruits < 150) {
      this.heightFruits = 150;
    }
  }

  getDay() {
    this._agua.get().subscribe();
    this._alimentos.getDay();
    this._exercicios.getDay();
  }

}
