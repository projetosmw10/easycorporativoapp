import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { ApiProvider } from '../../providers/api/api';
import { TabsProvider } from '../../providers/tabs/tabs';
import { GestaoCadastroPage } from '../gestao-cadastro/gestao-cadastro';
import { GestaoListagemPage } from '../gestao-listagem/gestao-listagem';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasConteudosPage } from '../categorias-conteudos/categorias-conteudos';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { AreaProvider } from '../../providers/area/area';
import { GestaoImagensPage } from '../gestao-imagens/gestao-imagens';

/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gestao-home',
  templateUrl: 'gestao-home.html'
})
export class GestaoHomePage {

  @ViewChild(Refresher) refresher: Refresher;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _area: AreaProvider,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider
  ) {
    if (!this._area.list.length) {
      this.loading.present();
    }
    this.onRefresher();
  }

  onRefresher() {
    console.log(this._area.getAll());
    this._area.getAll().then(() => {
      this.loading.dismiss()
      this.refresher.complete()
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })
  }

  ionViewDidEnter() {
    this._area.getAll().then(r => {
      this.loading.dismiss();
      this.refresher.complete()
    }, e => {
      this.loading.dismiss();
      this.refresher.complete()
    })
  }

  categoriasConteudosPage(item) {
    this._area.current = item;
    this.navCtrl.push(CategoriasConteudosPage);
  }

  cadastro() {
    this.navCtrl.push(GestaoCadastroPage)
  }

  listagem() {
    this.navCtrl.push(GestaoListagemPage)
  }

  imagens() {
    this.navCtrl.push(GestaoImagensPage);
  }


}
