import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, App } from 'ionic-angular';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { ApiProvider } from '../../providers/api/api';
import { TabsProvider } from '../../providers/tabs/tabs';
import { EasyfitSportsTabsPage } from '../easyfit-sports-tabs/easyfit-sports-tabs';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasConteudosPage } from '../categorias-conteudos/categorias-conteudos';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { AuthProvider } from '../../providers/auth/auth';
import { CadastroUsuarioPage } from '../cadastro-usuario/cadastro-usuario';
import { EasyfitNutriTabsPage } from '../easyfit-nutri-tabs/easyfit-nutri-tabs';

/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-easyfit-sports-home',
  templateUrl: 'easyfit-sports-home.html'
})
export class EasyfitSportsHomePage {

  @ViewChild(Refresher) refresher: Refresher;
  easyfitNutri = this.aplicativos.getEasyfitNutri()
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _categorias: CategoriasProvider,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider,
    public auth: AuthProvider,
    public app: App
  ) {
    if (!this._categorias.list.length) {
      this.loading.present();
    }
    this.onRefresher();
  }

  onRefresher() {
    this.ionViewDidEnter();
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this._categorias.getAll({ completo: false }).then(r => {
        this.loading.dismiss();
        this.refresher.complete()
      }, e => {
        this.loading.dismiss();
        this.refresher.complete()
      })
    }, 50)
  }

  categoriasConteudosPage(item) {
    this._categorias.current = item;
    this.navCtrl.push(CategoriasConteudosPage);
  }

  easyfitNutriPage() {
    if (!this.auth.user.user.inicio_kg) {
      this.app.getRootNav().setRoot(CadastroUsuarioPage, { animate: true, animation: 'ios-transition', duration: 1000 });
    } else {
      this.app.getRootNav().setRoot(EasyfitNutriTabsPage, { animate: true, animation: 'ios-transition', duration: 1000 })
    }
    this.loading.present()
    setTimeout(() => {

      this.aplicativos.setItem(this.easyfitNutri)
      this.loading.dismiss()
    }, 50)
  }

}
