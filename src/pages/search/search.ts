import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { PlayerPage } from '../player/player';
import { ApiProvider } from '../../providers/api/api';
import { LoadingProvider } from '../../providers/loading/loading';



@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  list = [];
  search = '';
  loading = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _categorias: CategoriasProvider,
    public api: ApiProvider,
    public _loading: LoadingProvider
  ) {
    this.search = this.navParams.get('search');
    this.onSearch(this.search.toLocaleLowerCase());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  onSearch(search) {
    this.search = search.toLocaleLowerCase();

    this._loading.present();
    this.loading = true;
    this.api.get('/conteudos/search?search=' + search).subscribe(
      r => {
        this._loading.dismiss();
        this.loading = false;
        this.list = r;
      },
      e => {
        this._loading.dismiss();
        this.loading = false
      }
    )

  }

  playerPage(item) {
    this.navCtrl.push(PlayerPage, { item: item });
  }

}
