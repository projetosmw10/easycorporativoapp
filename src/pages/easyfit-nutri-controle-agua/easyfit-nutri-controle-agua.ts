import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { ApiProvider } from '../../providers/api/api';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { EasyfitNutriAguaProvider } from '../../providers/easyfit-nutri-agua/easyfit-nutri-agua';
import { HelpersProvider } from '../../providers/helpers/helpers';

/**
 * Generated class for the ControleAguaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-easyfit-nutri-controle-agua',
  templateUrl: 'easyfit-nutri-controle-agua.html',
})
export class EasyfitNutriControleAguaPage {

  qtdAgua: number = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public api: ApiProvider,
    public _alimentos: EasyfitNutriAlimentosProvider,
    public _agua: EasyfitNutriAguaProvider,
    public helpers: HelpersProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ControleAguaPage');
  }
  ionViewWillEnter() {
    this._agua._get = this._agua.get().subscribe();
  }
  save() {
    if (parseInt(this._agua.item.quantidade) < this._agua.item.consumido) {
      this._agua.item.consumido = this._agua.item.quantidade; 
      console.log('blalblblba')
    }
    this._agua._save = this._agua.save().subscribe(r => this._agua.updateCoposView())
  }

}
