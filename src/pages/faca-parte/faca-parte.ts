import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Keyboard, AlertController, Platform } from 'ionic-angular';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { LoadingProvider } from '../../providers/loading/loading';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';
import { EsqueciSenhaPage } from '../esqueci-senha/esqueci-senha';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { useAnimation } from '@angular/core/src/animation/dsl';
import { NgForm } from '@angular/forms';

/**
 * Generated class for the FacaPartePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-faca-parte',
  templateUrl: 'faca-parte.html',
})
export class FacaPartePage {
  // @ViewChild('video') video: ElementRef;
  user = {
    first_name: '',
    telefone: '',
    email: '',
    password: '',
    confirmPassword: ''
  }
  showVideo = false;
  aceito = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public helpers: HelpersProvider,
    public _loading: LoadingProvider,
    public global: GlobalProvider,
    public http: HttpClient,
    public auth: AuthProvider,
    public keyboard: Keyboard,
    public alert: AlertController,
    public iab: InAppBrowser,
    public plt: Platform
  ) {
  }

  ionViewDidLoad() {

    // this.video.nativeElement.muted = true;
    // this.video.nativeElement.play();
    setTimeout(() => {
      this.showVideo = true;
    }, 2000);
  }
  termosDeUso() {
    if (!this.global.informacoes.termos_de_uso) {
      return;
    }
    if(this.plt.is('ios')) {
      this.iab.create(this.global.informacoes.termos_de_uso, 'system').show();
    } else {
      this.iab.create(this.global.informacoes.termos_de_uso, 'blank').show();
    }
  }
  onSubmit(form:NgForm) {
    this.helpers.formMarkAllTouched(form);
    if (!this.aceito) {
      this.alert.create({ title: 'Termos de Uso', message: 'Você deve aceitar os termos de uso e condição para poder se cadastrar' }).present();
      return;
    }
    if (!form.valid) {
      this.toastCtrl.create({ message: 'Verifique se todos os campos estão preenchidos corretamente', duration: 4000 }).present();
      return;
    }
    if (!this.helpers.isEmail(this.user.email)) {
      form.controls.email.setErrors({invalid: true});
      this.toastCtrl.create({ message: 'Informe um e-mail válido', duration: 4000 }).present();
      return
    }
    if (this.user.password.length < 6) {
      this.toastCtrl.create({ message: 'Informe uma senha com pelo menos 6 caracteres', duration: 4000 }).present();
      return;
    }
    if (this.user.password != this.user.confirmPassword) {
      this.toastCtrl.create({ message: 'As senhas não conferem', duration: 4000 }).present();
      return;
    }

    console.log(this.user);

    this._loading.present();
    this.http.post(this.global.apiUrl + '/register', this.user).subscribe(
      (r: any) => {
        this._loading.dismiss();
        this.toastCtrl.create({ message: this.user.first_name + ', sua conta foi criada!', duration: 4000 }).present();
        // Object.assign(this.loginPage.credentials, this.user);
        // setTimeout(() => this.loginPage.onSubmit(), 300);
        this.navCtrl.push(LoginPage, { user: this.user });
      },
      (e: HttpErrorResponse) => {
        this._loading.dismiss();
        if (e.status == 400) {
          this.toastCtrl.create({ message: e.error.message, duration: 4000 }).present();
        } else {
          this.toastCtrl.create({ message: 'Por favor, tente novamente', duration: 4000 }).present();
        }
      }
    )
  }

  esqueciSenhaPage() {
    this.navCtrl.pop();
    this.navCtrl.push(EsqueciSenhaPage);
  }

  backspaceTelefone() {
    if(this.user.telefone.length <= 3) {
      this.user.telefone = '';
    }
  }

}
