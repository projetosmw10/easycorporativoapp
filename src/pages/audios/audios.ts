import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';

import { CategoriasProvider } from '../../providers/categorias/categorias';

/**
 * Generated class for the AudiosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-audios',
  templateUrl: 'audios.html',
})
export class AudiosPage {

  @ViewChild(Refresher) refresher: Refresher;
  conteudos = [];
  audios = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _categorias: CategoriasProvider
  ) {
    this.onlyAudios();
  }

  onRefresher() {
    this._categorias.getAll().then(() => {
      this.refresher.complete();
      this.onlyAudios();
    }, () => this.refresher.complete())
  }

  onlyAudios() {
    this.audios = this._categorias.list.map(categoria => {
      categoria.topicos = categoria.topicos.map(topico => {
        topico.conteudos = topico.conteudos.filter(conteudo => conteudo.tipo == 'audio');

        topico.topicos = topico.topicos.map(topico2 => {
          topico2.conteudos = topico2.conteudos.filter(conteudo => conteudo.tipo == 'audio');
          return topico2
        })
        
        return topico
      })
      return categoria
    })
  }

  toggleItem(i) {
    this.audios = this.audios.map(
      (obj, index) => {

        //abre aquele especifico
        if (index == i) {
          obj.open = !obj.open;
        } else {
          obj.open = false;
        }
        return obj;
      }
    )
    this.conteudos = this.audios[i].topicos;
  }
}
