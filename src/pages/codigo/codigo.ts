import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import { NgForm } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';
import { AuthProvider } from '../../providers/auth/auth';
import { HttpErrorResponse } from '@angular/common/http';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { GlobalProvider } from '../../providers/global/global';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the CodigoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-codigo',
  templateUrl: 'codigo.html',
})
export class CodigoPage {
  code = '';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingProvider,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public auth: AuthProvider,
    public alertCtrl: AlertController,
    public helpers: HelpersProvider,
    public global: GlobalProvider,
    public _categorias: CategoriasProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CodigoPage');
  }

  ionViewWillEnter() {
    this.api.post('/users/' + this.auth.user.user.id, {}, false).subscribe(r => this.auth.setToken(r.token))
  }
  onSubmit(form: NgForm) {
    if (this.code.length == 0) {
      this.toastCtrl.create({ message: 'Você deve informar um código', duration: 4000 }).present();
      return
    }

    this.loading.present();
    this.api.post('/users/' + this.auth.user.user.id + '/premium', { code: this.code }, false).subscribe(
      r => {
        this.loading.dismiss();
        this.code = '';
        console.log(this.auth.user.user);
        this.auth.setToken(r.token);
        console.log(this.auth.user.user);
        this.alertCtrl.create({
          title: 'Sucesso!', message: 'Seu código foi ativado com sucesso, ele irá expirar em ' + this.helpers.moment(this.auth.user.user.premium_expire_at, 'YYYY-MM-DD HH:mm:ss').format('LLL'), buttons: [
            { text: 'Ok' }
          ]
        }).present();
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop();
        }
        this._categorias.getAll();

      },
      (e: HttpErrorResponse) => {
        this.loading.dismiss();
        this.code = '';
        console.log(e);
        if (e.error.message) {
          this.toastCtrl.create({ message: e.error.message, duration: 4000 }).present();
        } else {
          this.toastCtrl.create({ message: 'Um erro desconhecido aconteceu, por favor tente novamente', duration: 4000 }).present()
        }
      }
    )

  }


}
