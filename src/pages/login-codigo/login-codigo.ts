import { Component } from '@angular/core';
import { NavController, NavParams, Keyboard, ToastController, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
import { GlobalProvider } from '../../providers/global/global';
import { ApiProvider } from '../../providers/api/api';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { LoadingProvider } from '../../providers/loading/loading';
import { HttpClient } from '@angular/common/http';
import { AplicativosPage } from '../aplicativos/aplicativos';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the LoginCodigoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login-codigo',
  templateUrl: 'login-codigo.html',
})
export class LoginCodigoPage {
  obj = {
    identificador: '',
    code: ''
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public global: GlobalProvider,
    public keyboard: Keyboard,
    public toastCtrl: ToastController,
    public loading: LoadingProvider,
    public http: HttpClient,
    public helpers: HelpersProvider,
    public alertCtrl: AlertController,
    public statusBar: StatusBar
  ) {
    this.statusBar.backgroundColorByHexString('#000000');
    if(this.helpers.isAppleReview()) {
      this.navCtrl.setRoot(AplicativosPage);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginCodigoPage');
    const obj = window.localStorage.getItem('login-codigo');
    if(obj) {
      this.obj = JSON.parse(obj);
    }
    
  }

  onSubmit(form) {
    if (form.invalid) {
      this.toastCtrl.create({ message: 'Preencha todos os campos', duration: 2000 }).present();
      return
    }

    this.loading.present();
    window.localStorage.setItem('login-codigo', JSON.stringify(this.obj));
    this.http.post(this.global.apiUrl + '/users/' + this.auth.user.user.id + '/premium', this.obj, {
      headers: this.auth.getAuthorizationHeader()
    }).subscribe((r: any) => {
      this.auth.setToken(r.token)
      this.loading.dismiss();
      this.navCtrl.setRoot(AplicativosPage, {}, { animate: true, duration: 1000, animation: 'wp-transition', direction: 'forward' });
    }, e => {
      if(e.status == 400) {
        this.alertCtrl.create({title: 'Oops...', message: e.error.message}).present()
      }
      if(e.status == 401) {
        this.alertCtrl.create({
          title: 'Sessão Expirada',
          message: 'Você precisa logar novamente para continuar usando o aplicativo',
          cssClass: 'text-danger',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'OK',
              handler: () => {
    
                this.navCtrl.setRoot(LoginPage).then(() => this.auth.logout())
              },
              role: 'cancel'
            }
          ]
        }).present()
      }
      this.loading.dismiss();
    })
  }
  logout() {
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, duration: 1000, animation: 'wp-transition', direction: 'forward' }).then(() => this.auth.logout());
  }

}
