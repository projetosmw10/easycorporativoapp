import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { ApiProvider } from '../../providers/api/api';
import { LoadingProvider } from '../../providers/loading/loading';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';

/**
 * Generated class for the ContatoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contato',
  templateUrl: 'contato.html',
})
export class ContatoPage {
  contato: any = {
    nome: this.auth.user.user.first_name,
    telefone: this.auth.user.user.telefone,
    email: this.auth.user.user.email,
    mensagem: ''
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public helpers: HelpersProvider,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContatoPage');
  }

  onSubmit(form: NgForm) {
    if(this.aplicativos.item.id) {
      this.contato.aplicativo_id = this.aplicativos.item.id;
    } else {
      this.contato.aplicativo_id = null;
    }
    if (!this.helpers.isEmail(this.contato.email)) {
      this.toastCtrl.create({ message: 'Digite um e-mail válido', duration: 4000 }).present();
      return
    }
    if (!form.valid) {
      this.toastCtrl.create({ message: 'Verifique se todos os campos estão preenchidos', duration: 4000 }).present();
      return
    }
    this.loading.present();
    this.api.post('/contato', this.contato).subscribe(r => {
      this.loading.dismiss()
      this.toastCtrl.create({ message: 'Sua mensagem foi enviada, responderemos o mais breve possível!', duration: 4000 }).present();
      this.contato = {
        nome: this.auth.user.user.first_name,
        telefone: this.auth.user.user.telefone,
        email: this.auth.user.user.email,
        mensagem: ''
      }
    }, e => this.loading.dismiss())
  }

}
