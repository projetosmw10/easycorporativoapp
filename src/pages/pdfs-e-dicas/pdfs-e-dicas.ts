import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, ToastController, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { LoadingProvider } from '../../providers/loading/loading';
import { Subject } from 'rxjs/Subject';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { PdfsProvider } from '../../providers/pdfs/pdfs';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';

/**
 * Generated class for the PdfsEDicasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pdfs-e-dicas',
  templateUrl: 'pdfs-e-dicas.html',
})
export class PdfsEDicasPage {

  @ViewChild(Refresher) refresher: Refresher;

  active = this._pdfs.list.length ? 'pdf' : 'dicas';
  dicas: any = [];
  pdfs: any = [];
  loading = false;

  requests = new Subject<number>();
  requestsCompleted = 0;
  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public api: ApiProvider,
    public _loading: LoadingProvider,
    public iap: InAppBrowser,
    public alertCtrl: AlertController,
    public _pdfs: PdfsProvider,
    public _aplicativos: AplicativosProvider
  ) {
    this.requests.subscribe(() => {
      if (this.requestsCompleted == 2) {
        this.refresher.complete();
        this._loading.dismiss();
        this.loading = false;
        this.requestsCompleted = 0;
      }
    })

    this.onRefresher();
  }

  onRefresher() {
    if (!(this.dicas.length) || !(this.pdfs.length)) {
      this._loading.present()
    }

    this.loading = true;
    this.api.get('/dicas?aplicativo_id='+this._aplicativos.item.id).subscribe(
      r => {
        this.requests.next(this.requestsCompleted++);
        this.dicas = r;
      },
      e => this.requests.next(this.requestsCompleted++)
    )

    this._pdfs.getAll().subscribe(
      r => this.requests.next(this.requestsCompleted++),
      e => this.requests.next(this.requestsCompleted++)
    )

  }




  openTab(tab) {
    this.active = tab;
  }
  toggleDica(i) {
    this.dicas[i].active = !this.dicas[i].active;
  }
}
