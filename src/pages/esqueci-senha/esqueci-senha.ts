import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ToastController, Keyboard } from 'ionic-angular';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { LoadingProvider } from '../../providers/loading/loading';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthProvider } from '../../providers/auth/auth';
import { ApiProvider } from '../../providers/api/api';
import { FacaPartePage } from '../faca-parte/faca-parte';

/**
 * Generated class for the EsqueciSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-esqueci-senha',
  templateUrl: 'esqueci-senha.html',
})
export class EsqueciSenhaPage {
  // @ViewChild('video') video: ElementRef;
  email = ''
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public helpers: HelpersProvider,
    public _loading: LoadingProvider,
    public global: GlobalProvider,
    public http: HttpClient,
    public keyboard: Keyboard
  ) {
  }

  ionViewDidLoad() {
    // this.video.nativeElement.muted = true;
    console.log('ionViewDidLoad EsqueciSenhaPage');
  }

  facaPartePage() {
    this.navCtrl.pop();
    this.navCtrl.push(FacaPartePage);
  }

  onSubmit() {
    if (!this.helpers.isEmail(this.email)) {
      this.toastCtrl.create({ message: 'Informe um e-mail válido', duration: 4000 }).present();
      return
    }
   this._loading.present();
    this.http.post(this.global.apiUrl + '/forgot-password', { email: this.email }).subscribe(
      r => {
        this._loading.dismiss();
        this.toastCtrl.create({ message: 'Um e-mail foi enviado para você!', duration: 4000 }).present();
        this.navCtrl.pop();
      },
      (e: HttpErrorResponse) => {
        this._loading.dismiss();
        if (e.status == 404) {
          this.toastCtrl.create({ message: 'Verifique se seu e-mail foi digitado corretamente', duration: 4000 }).present();
        } else {
          this.toastCtrl.create({ message: 'Por favor, tente novamente', duration: 4000 }).present();
        }
      }
    )
  }

}
