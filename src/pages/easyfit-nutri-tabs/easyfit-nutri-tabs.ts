import { HelpersProvider } from './../../providers/helpers/helpers';
import { ContactPage } from './../contact/contact';
import { AboutPage } from './../about/about';
import { PerfilPage } from './../perfil/perfil';
import { Component, ViewChild } from '@angular/core';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

import { Tabs, NavParams } from 'ionic-angular';
import { EasyfitNutriHomePage } from '../easyfit-nutri-home/easyfit-nutri-home';
import { EasyfitNutriControleAguaPage } from './../easyfit-nutri-controle-agua/easyfit-nutri-controle-agua';
// import { EasyfitNutriDicasPage } from './../dicas/dicas';
import { EasyfitNutriCalculoCaloricoPage } from './../easyfit-nutri-calculo-calorico/easyfit-nutri-calculo-calorico';
import { EasyfitNutriRefeicoesPage } from './../easyfit-nutri-refeicoes/easyfit-nutri-refeicoes';
import { SplashScreen } from '@ionic-native/splash-screen';
import { EasyfitNutriExerciciosPage } from '../easyfit-nutri-exercicios/easyfit-nutri-exercicios';

@Component({
  templateUrl: 'easyfit-nutri-tabs.html'
})
export class EasyfitNutriTabsPage {

  @ViewChild('tabRef') tabRef: Tabs;

  tab0Root = PerfilPage;
  tab1Root = EasyfitNutriRefeicoesPage;
  tab2Root = EasyfitNutriControleAguaPage;
  tab3Root = EasyfitNutriHomePage;
  tab4Root = EasyfitNutriExerciciosPage;
  tab5Root = EasyfitNutriCalculoCaloricoPage;
  tab6Root = AboutPage;
  tab7Root = ContactPage;
  // tab8Root = DicasPage;

  tabIndex: number = 0;
  select;

  constructor(
    public nativePageTransitions: NativePageTransitions,
    public navParams: NavParams,
    public splashScreen: SplashScreen
  ) {
    this.select = this.navParams.get('select');
  }

  ionViewDidLoad() {
    setTimeout(() => this.splashScreen.hide(), 600);

    if (this.select !== undefined) {
      this.tabRef.select(this.select);
    }
  }

  getAnimationDirection(index) {
    var currentIndex = this.tabIndex;

    this.tabIndex = index;

    switch (true) {
      case (currentIndex < index):
        return ('left');
      case (currentIndex > index):
        return ('right');
    }
  }

  transition(e) {
    let options: NativeTransitionOptions = {
      direction: this.getAnimationDirection(e.index),
      duration: 300,
      slowdownfactor: -.5,
      slidePixels: 0,
      iosdelay: 10,
      androiddelay: 10,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0,


    };

    // if (!this.loaded) {
    //   this.loaded = true;
    //   return;
    // }

    this.nativePageTransitions.slide(options);
  }
}
