import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController, Refresher } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { AuthProvider } from '../../providers/auth/auth';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { LoadingProvider } from '../../providers/loading/loading';
import { PlayerPage } from '../player/player';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';

/**
 * Generated class for the FavoritosPage page.
 *
 * See https://ionicframework.com/docsodel/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html',
})
export class FavoritosPage {

  @ViewChild(Refresher) refresher: Refresher;

  videos_open = true;
  audios_open = false;
  pdfs_open = false;
  videos = [];
  audios = [];
  pdfs = [];
  conteudos = [];
  loading = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public auth: AuthProvider,
    public toastCtrl: ToastController,
    public _categorias: CategoriasProvider,
    public _loading: LoadingProvider,
    public aplicativos: AplicativosProvider
  ) {
  }

  // cada vez q a tab é aberta ele busca na api os favoritos do usuario

  ionViewWillEnter() {
    this.onRefresher();
    console.log('ionViewDidLoad AudiosPage');
  }

  onRefresher() {
    if (!(this.videos.length || this.audios.length)) {
      this._loading.present()
    }
    this.loading = true;
    this.api.get('/users/' + this.auth.user.user.id + '/favoritos?aplicativo_id=' + this.aplicativos.item.id).subscribe(
      r => {
        this.refresher.complete();
        this._loading.dismiss();
        this.loading = false;
        this.videos = r.filter(obj => obj.tipo == 'video');
        this.conteudos = this.videos;
        this.audios = r.filter(obj => obj.tipo == 'audio');
        this.pdfs = r.filter(obj => obj.tipo == 'pdf');
      },
      e => this._loading.dismiss()
    )
  }

  playerPage(item) {
    this.navCtrl.push(PlayerPage, { item: item });
  }

  favoritar(item) {
    this.videos = this.videos.filter(obj => obj.id != item.id);
    this.audios = this.audios.filter(obj => obj.id != item.id);
    this.pdfs = this.pdfs.filter(obj => obj.id != item.id);
    this.conteudos = this.conteudos.filter(obj => obj.id != item.id);
    // this._categorias.favoritar(item);
  }
}
