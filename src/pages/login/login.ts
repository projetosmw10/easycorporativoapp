import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, Keyboard, ToastController } from 'ionic-angular';
import { FacaPartePage } from '../faca-parte/faca-parte';
import { AuthProvider } from '../../providers/auth/auth';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { EsqueciSenhaPage } from '../esqueci-senha/esqueci-senha';
import { GlobalProvider } from '../../providers/global/global';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClient } from '@angular/common/http';
import { LoginCodigoPage } from '../login-codigo/login-codigo';
import { EasymindTabsPage } from '../easymind-tabs/easymind-tabs';
import { AplicativosPage } from '../aplicativos/aplicativos';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  // @ViewChild('video') video: ElementRef;
  user: any = {
    email: '',
    password: ''
  }
  showVideo = false;
  constructor(
    public global: GlobalProvider,
    public helpers: HelpersProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public loading: LoadingProvider,
    public toastCtrl: ToastController,
    public keyboard: Keyboard,
    public splashScreen: SplashScreen,
    public http: HttpClient,
    public aplicativos: AplicativosProvider,
    public statusBar: StatusBar
  ) {
    if (this.navParams.get('user')) {
      this.user = this.navParams.get('user');
      this.onSubmit();
    }
    this.statusBar.backgroundColorByHexString('#000000');
  }

  ionViewDidLoad() {
    this.aplicativos.clearItem();
    // this.video.nativeElement.muted = true;
    // this.video.nativeElement.play();
    setTimeout(() => {
      this.showVideo = true;
      setTimeout(() => this.splashScreen.hide(), 600);
    }, 3000);
  }

  onSubmit(form?) {
    if(form) {
      this.helpers.formMarkAllTouched(form)
    }
    if (!(this.user.email || this.user.password)) {
      this.toastCtrl.create({ message: 'Por favor, informe seu e-mail e sua senha', duration: 3000 }).present();
      return;
    }


    this.loading.present();
    this.auth.login(this.user).subscribe(
      (res: any) => {
        this.auth.setToken(res.token);
        this.loading.dismiss();

        if (!this.navParams.get('user')) {
          this.toastCtrl.create({ message: 'Olá, ' + this.auth.user.user.first_name + ', seja bem vindo novamente!', duration: 3000 }).present();
        } else {
          setTimeout(() => {
            this.toastCtrl.create({ message: 'Olá, ' + this.auth.user.user.first_name + ', seja bem vindo!', duration: 3000 }).present();
          }, 3000)
        }

        if (this.auth.isPremium()) {
          this.navCtrl.setRoot(AplicativosPage, {}, { animate: true, animation: 'md-transition' });
        } else {
          this.navCtrl.setRoot(LoginCodigoPage, {}, { animate: true, animation: 'md-transition' })
        }

      },
      () => {
        this.toastCtrl.create({ message: 'Credenciais inválidas, por favor tente novamente', duration: 3000 }).present();
        this.loading.dismiss();
      }
    );

  }

  tabsPage() {
    this.navCtrl.setRoot(EasymindTabsPage, {}, { animate: true, animation: 'md-transition' })
  }
  facaPartePage() {
    this.navCtrl.push(FacaPartePage)
  }
  esqueciSenhaPage() {
    this.navCtrl.push(EsqueciSenhaPage)
  }


}
