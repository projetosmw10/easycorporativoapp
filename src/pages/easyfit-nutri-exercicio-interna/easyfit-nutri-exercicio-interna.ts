import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ToastController, Content } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { AuthProvider } from '../../providers/auth/auth';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { EasyfitNutriExerciciosProvider } from '../../providers/easyfit-nutri-exercicios/easyfit-nutri-exercicios';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { LoadingProvider } from '../../providers/loading/loading';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the ExercicioInternaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-easyfit-nutri-exercicio-interna',
  templateUrl: 'easyfit-nutri-exercicio-interna.html',
})
export class EasyfitNutriExercicioInternaPage {
  heightFruits = 0;
  @ViewChild(Content) content: Content;
  @ViewChild('layout') layout: ElementRef;
  
  item: any = {}
  energia;

  quantidade;

  atualizar;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public auth: AuthProvider,
    public helpers: HelpersProvider,
    public _exercicios: EasyfitNutriExerciciosProvider,
    public _alimentos: EasyfitNutriAlimentosProvider,
    public toastCtrl: ToastController,
    public loading: LoadingProvider,
    public global: GlobalProvider
  ) {
    this.item = this.navParams.get('item');


    this.atualizar = this.navParams.get('atualizar');
    this.quantidade = this.navParams.get('quantidade');

    this.energia = this.item.energia;

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RefeicaoInternaPage');
    this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
    if(this.heightFruits < 150) {
      this.heightFruits = 150;
    }
  }

  salvar() {
    if (!this.quantidade || this.quantidade < 0) {
      this.toastCtrl.create({ message: 'Por favor, informe a quantidade desejada', duration: 4000 }).present();
      return;
    }
    this.loading.present();
    this.request().subscribe(r => {
      this.toastCtrl.create({ message: 'Atividade salva para o dia ' + this.helpers.moment(this._alimentos.selectedDate, 'YYYY-MM-DD').format('LL'), duration: 4000 }).present();
      this._exercicios.getDay().then(() => {
        this.navCtrl.pop();
        this.loading.dismiss();
      });
      ;
    })
  }


  calcular() {
    this.energia = Math.ceil((this.item.energia * this.quantidade) / 60);
  }


  request() {
    if (this.atualizar) {
      return this.api.patch('/users/' + this.auth.user.user.id + '/exercicios', {
        exercicio_id: this.item.id,
        data: this._alimentos.selectedDate,
        quantidade: this.quantidade,
      })
    } else {
      return this.api.post('/users/' + this.auth.user.user.id + '/exercicios', {
        exercicio_id: this.item.id,
        data: this._alimentos.selectedDate,
        quantidade: this.quantidade,
      })
    }
  }

}
