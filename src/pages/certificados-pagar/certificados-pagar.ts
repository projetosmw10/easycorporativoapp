import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { AuthProvider } from '../../providers/auth/auth';
import { ApiProvider } from '../../providers/api/api';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { NgForm } from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the CertificadosPagarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-certificados-pagar',
  templateUrl: 'certificados-pagar.html',
})
export class CertificadosPagarPage {
  cc = {
    name: '',
    number: '',
    month: '',
    year: '',
    cvv: ''
  }
  emailsArray = [this.auth.user.user.email]
  nome = ''
  categoria
  certificado: any = {}
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public helpers: HelpersProvider,
    public auth: AuthProvider,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public alertCtrl: AlertController,
    public categorias: CategoriasProvider,
    public global: GlobalProvider,
    public plt: Platform) {
    this.init()
  }

  init() {
    this.getCertificados();
    this.categoria = this.navParams.get('categoria');
    if (!this.categoria) {
      this.navCtrl.pop()
      return
    }
    this.categorias.get('/categorias/' + this.categoria.id).subscribe(r => {
      if(r.conteudos_assistido_count != r.conteudos_assistido_count) {
        this.navCtrl.pop()
        this.helpers.toast('Você precisa ver todos os conteúdos antes de poder fazer o download do certificado')
      }
    }, e => {
      this.navCtrl.pop()
    })
  }


  getCertificados() {
    this.loading.present();
    this.api.get('/certificados').subscribe(r => {
      console.log(r)
      this.certificado = r[0]
      this.loading.dismiss()
    }, e => {
      this.loading.dismiss()
      this.alertCtrl.create({
        title: 'Ops...',
        message: 'Não foi possível conectar-se aos servidor da EasyLife, deseja tentar novamente?',
        buttons: [
          {
            role: 'cancel',
            text: 'Não',
            handler: () => {
              this.navCtrl.pop()
            }
          },
          {
            text: 'Sim',
            handler: () => {
              this.getCertificados()
            }
          }
        ]
      }).present()
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CertificadosPagarPage');
  }


  onSubmit(form: NgForm) {
    this.helpers.formMarkAllTouched(form);
    if (!this.nome.trim().length) {
      return this.helpers.toast('Preencha o nome que será colocado no certificado')
    }
    if (!this.emailsArray[0].length) {
      return this.helpers.toast('Coloque pelo menos 1 e-mail que será enviado o certificado')
    }
    if (this.cc.name.split(' ').length < 2) {
      return this.helpers.toast('Preencha o nome completo que está no cartão de crédito')
    }
    if (!this.cc.month.trim().length) {
      return this.helpers.toast('Preencha o mês de validade do cartão de crédito')
    }
    if (this.cc.year.length != 2) {
      return this.helpers.toast('Preencha o ano de validade do cartão de crédito')
    }
    if (!this.cc.cvv.length) {
      return this.helpers.toast('Preencha o código de segurança do cartão de crédito')
    }

    if (!form.valid) {
      return this.helpers.toast('Verifique os campos em vermelho')
    }
    this.loading.present()
    this.api.post('/certificados/pagar', {
      nome: this.nome,
      cc: this.cc,
      emails: this.emailsArray.join(','),
      categoria_id: this.categoria.id,
      certificado_id: this.certificado.id
    }).subscribe(r => {
      this.loading.dismiss()
      this.alertCtrl.create({
        message: r.message,
        buttons: [
          {
            text: 'Ver certificado',
            handler: () => {
              if(this.plt.is('ios')) {
                window.open(r.link, '_system')
              } else {
                window.open(r.link, '_blank')
              }
            }
          }
        ]
      }).present()
      this.navCtrl.pop()
    }, e => {
      this.loading.dismiss()
    })
  }



}
