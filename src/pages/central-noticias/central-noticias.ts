import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { ApiProvider } from '../../providers/api/api';
import { TabsProvider } from '../../providers/tabs/tabs';
import { GestaoCadastroPage } from '../gestao-cadastro/gestao-cadastro';
import { LoadingProvider } from '../../providers/loading/loading';
import { CategoriasConteudosPage } from '../categorias-conteudos/categorias-conteudos';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { AreaProvider } from '../../providers/area/area';
import { NoticiasProvider } from '../../providers/noticias/noticias';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { CentralNoticiasInternaPage } from '../central-noticias-interna/central-noticias-interna';
/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-central-noticias',
  templateUrl: 'central-noticias.html'
})
export class CentralNoticiasPage {

  @ViewChild(Refresher) refresher: Refresher;
  area_id = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider,
    public _noticias: NoticiasProvider,
    public helpers: HelpersProvider
  ) {
    if (!this._noticias.list.length) {
      this.loading.present();
    }
    this.onRefresher();
  }

  onRefresher() {   

    this._noticias.getAll().then(() => {
      this.loading.dismiss()
      this.refresher.complete()
      
    }, () => {
      this.loading.dismiss()
      this.refresher.complete()
    })
  }


  ionViewDidEnter() {
    this._noticias.getAll().then(r => {
      this.loading.dismiss();
      this.refresher.complete()
    }, e => {
      this.loading.dismiss();
      this.refresher.complete()
    })
  }

  interna(item) {
    
    this.navCtrl.push(CentralNoticiasInternaPage, {item: item, parentPage: this})
  }

}
