import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';
import { LoadingProvider } from '../../providers/loading/loading';
import { EasymindTabsPage } from '../easymind-tabs/easymind-tabs';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { EasyfitSportsTabsPage } from '../easyfit-sports-tabs/easyfit-sports-tabs';
import { AuthProvider } from '../../providers/auth/auth';
import { CadastroUsuarioPage } from '../cadastro-usuario/cadastro-usuario';
import { EasyfitNutriTabsPage } from '../easyfit-nutri-tabs/easyfit-nutri-tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { LoginCodigoPage } from '../login-codigo/login-codigo';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { LoginPage } from '../login/login';
import { GestaoTabsPage } from '../gestao-tabs/gestao-tabs';
import { CentralTabsPage } from '../central-tabs/central-tabs';


@Component({
  selector: 'page-aplicativos',
  templateUrl: 'aplicativos.html',
})
export class AplicativosPage {
  constructor(
    public navCtrl: NavController,
    public loading: LoadingProvider,
    public aplicativos: AplicativosProvider,
    public helpers: HelpersProvider,
    public auth: AuthProvider,
    public statusBar: StatusBar,
    public alertCtrl: AlertController,
    public _categorias: CategoriasProvider
  ) {
  }

  ionViewWillEnter() {
    this.aplicativos.clearItem();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.auth.isPremiumAsync().catch(r => {
      if (!this.helpers.isAppleReview()) {
        if (r.status == 401) {
          this.alertCtrl.create({
            title: 'Sessão Expirada',
            message: 'Você precisa logar novamente para continuar usando o aplicativo',
          }).present().then(r => {
            this.navCtrl.setRoot(LoginPage).then(r => this.auth.logout());
          })
        } else {
          this.alertCtrl.create({ title: 'Código expirado', message: 'Seu código expirou' }).present().then(r => {
            this.navCtrl.setRoot(LoginCodigoPage);
          })
        }
      }
    })
    this.aplicativos.getAll().then(() => this.loading.dismiss()).catch(() => this.loading.dismiss())
  }

  setItem(item) {
    this.aplicativos.setItem(item);
    
    
    setTimeout(() => {
      if (item.id == 1) {
        this.navCtrl.push(EasymindTabsPage);
      } else if (item.id == 2) {
        this.navCtrl.push(EasyfitSportsTabsPage);
      } else if (item.id == 3) {
        if (!this.auth.user.user.inicio_kg) {
          this.navCtrl.push(CadastroUsuarioPage);
        } else {
          this.navCtrl.push(EasyfitNutriTabsPage)
        }
      }
      else if (item.id == 5) {
        this.navCtrl.push(GestaoTabsPage);
      }
      else if (item.id == 6) {
        this.navCtrl.push(CentralTabsPage);
      }
      else {
        
        this.navCtrl.push(EasyfitSportsTabsPage);
      }
    }, 50)


    // limpa alguns dados que estavam salvos anteriormente
    this._categorias.current = {}
    this._categorias.list = [];
  }

}
