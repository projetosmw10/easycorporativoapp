import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, Platform } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { UploadsProvider } from '../../providers/uploads/uploads';
import { ApiProvider } from '../../providers/api/api';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { AuthProvider } from '../../providers/auth/auth';
import { AplicativosProvider } from '../../providers/aplicativos/aplicativos';

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  dados: any;
  hideButtons = false;
  barraObjetivo = 0;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private alertCtrl: AlertController,
    private camera: Camera,
    private imagePicker: ImagePicker,
    private uploads: UploadsProvider,
    private auth: AuthProvider,
    private loading: LoadingProvider,
    private api: ApiProvider,
    private toastCtrl: ToastController,
    public plt: Platform,
    public aplicativos: AplicativosProvider
  ) {
    this.hideButtons = this.navParams.get('hideButtons');
    this.dados = Object.assign({}, this.auth.user.user);
    this.barraObjetivoChange(this.dados.objetivo_kg);
    console.log(this.aplicativos.item);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  barraObjetivoChange(objetivo_kg) {
    this.dados.objetivo_kg = objetivo_kg;
    if (!this.dados.objetivo_kg) {
      this.dados.objetivo_kg = 1;
    }


    this.barraObjetivo = (this.dados.atual_kg / this.dados.objetivo_kg) * 100;

    if (this.dados.atual_kg > this.dados.objetivo_kg) {
      this.barraObjetivo = this.barraObjetivo - 100;
    }


  }

  onSubmit() {
    if (this.dados.inicio_kg) {
      if (!this.dados.objetivo_kcal) {
        this.toastCtrl.create({ message: 'Informe o seu objetivo (kcal)', duration: 4000 }).present();
        return
      }

      if (this.dados.objetivo_kg < 10 || !this.dados.objetivo_kg) {
        this.toastCtrl.create({ message: 'Informe o seu objetivo (kg)', duration: 4000 }).present();
        return
      }

      if (!this.dados.atual_kg || !(this.dados.atual_kg < 300 && this.dados.atual_kg > 0)) {
        this.toastCtrl.create({ message: 'Qual o seu peso?', duration: 3000 }).present();
        return;
      }
      if (!this.dados.first_name) {
        this.toastCtrl.create({ message: 'Informe o seu nome', duration: 4000 }).present();
        return
      }
      if (!this.dados.telefone) {
        this.toastCtrl.create({ message: 'Informe o seu telefone', duration: 4000 }).present();
        return
      }
      if (!this.dados.telefone) {
        this.toastCtrl.create({ message: 'Informe o seu telefone', duration: 4000 }).present();
        return
      }
    }
    if (this.dados.password) {
      if (this.dados.password.length < 6) {
        this.toastCtrl.create({ message: 'Informe uma senha com pelo menos 6 caracteres', duration: 4000 }).present();
        return;
      }
      if (this.dados.password != this.dados.confirmPassword) {
        this.toastCtrl.create({ message: 'As senhas não conferem, por favor tente novamente', duration: 400 }).present();
        return
      }
    } else {
      delete this.dados.password;
    }

    this.loading.present();
    this.api.post('/users/' + this.auth.user.user.id, this.dados).subscribe(
      r => {
        this.toastCtrl.create({ message: 'Dados alterados com sucesso!', duration: 4000 }).present();
        this.loading.dismiss();
        this.auth.setToken(r.token);
        this.dados = this.auth.user.user;
      },
      e => this.loading.dismiss()
    )
  }

  enviarFoto() {
    if (!this.plt.is('cordova')) {
      return;
    }
    const alert = this.alertCtrl.create({
      title: 'Alterar foto',
      buttons: [
        {
          text: 'Galeria',
          handler: () => this.openGallery()
        },
        {
          text: 'Camera',
          handler: () => this.openCamera()
        }
      ]
    })
    alert.present()
  }

  openGallery() {
    this.imagePicker.getPictures({
      quality: 80,
      width: 1280,
      maximumImagesCount: 1
    }).then(r => {
      this.uploads.startMultipleUpload(r, '/users/' + this.auth.user.user.id + '/avatar').then(
        r => {
          this.auth.setToken(JSON.parse(r[0].response).token);
          this.dados = this.auth.user.user;
        }
      ).catch(e => console.error('multiupload error', e))
    }).catch(e => console.error('imagePicker error', e));
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    }

    this.camera.getPicture(options)
      .then(r => {
        this.uploads.startMultipleUpload(r, '/users/' + this.auth.user.user.id + '/avatar').then(r => {
          this.auth.setToken(JSON.parse(r[0].response).token);
          this.dados = this.auth.user.user;
        })
      })
      .catch(e => console.error(' openCamera => e', e));
  }

}
