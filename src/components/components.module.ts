import { NgModule } from '@angular/core';
import { SublistItemComponent } from './sublist-item/sublist-item';

import { SublistItemDesktopComponent } from './sublist-item-desktop/sublist-item-desktop';
import { TabsComponent } from './tabs/tabs';
import { ConsumoAguaComponent } from './consumo-agua/consumo-agua';
import { HomeTabbarComponent } from './home-tabbar/home-tabbar';
import { RadioWeekComponent } from './radio-week/radio-week';
@NgModule({
	declarations: [SublistItemComponent,
    SublistItemDesktopComponent,
    TabsComponent,
    ConsumoAguaComponent,
    HomeTabbarComponent,
    RadioWeekComponent],
	imports: [],
	exports: [SublistItemComponent,
    SublistItemDesktopComponent,
    TabsComponent,
    ConsumoAguaComponent,
    HomeTabbarComponent,
    RadioWeekComponent]
})
export class ComponentsModule {}
