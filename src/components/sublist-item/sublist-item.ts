import { Component, Input, EventEmitter, Output } from '@angular/core';
import { NavController, AlertController, Platform, LoadingController } from 'ionic-angular';
import { PlayerPage } from '../../pages/player/player';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { AuthProvider } from '../../providers/auth/auth';
import { GlobalProvider } from '../../providers/global/global';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { CodigoPage } from '../../pages/codigo/codigo';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import 'mime-types';
import { CategoriasProvider } from '../../providers/categorias/categorias';
import { PlayerProvider } from '../../providers/player/player';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingProvider } from '../../providers/loading/loading';
/**
 * Generated class for the SublistItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sublist-item',
  templateUrl: 'sublist-item.html'
})
export class SublistItemComponent {
  @Output('favoritar') favoritarEvent = new EventEmitter<any>();
  @Input() item: any = {};
  @Input() forcarFavorito = false;

  constructor(
    public _categorias: CategoriasProvider,
    public navCtrl: NavController,
    public iap: InAppBrowser,
    public auth: AuthProvider,
    public alert: AlertController,
    public global: GlobalProvider,
    public helpers: HelpersProvider,
    public plt: Platform,
    public document: DocumentViewer,
    public player: PlayerProvider,
    public sanitizer: DomSanitizer,
    public loading: LoadingProvider
  ) {
  }

  playerPage() {
    this.loading.present().then(() => setTimeout(() => this.loading.dismiss(), 200))
    this.player.playerPage(this.item);
  }

  descricao() {
    this.alert.create({
      title: this.item.titulo, message: this.item.descricao.replace(/(?:\r\n|\r|\n)/g, '<br>'), cssClass: 'alert-lg', buttons: [{
        role: 'cancel',
        text: 'Ok'
      }]
    }).present();
  }

  favoritar() {
    this._categorias.favoritar(this.item).then(() => {
      this.favoritarEvent.next();
    });
    if (this.forcarFavorito) {
      this.item.favoritado = this.item.favoritado ? 0 : 1;
    }
  }
}
