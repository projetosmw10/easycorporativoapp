import { Component } from '@angular/core';
import { HelpersProvider } from '../../providers/helpers/helpers';
import { AuthProvider } from '../../providers/auth/auth';
import { EasyfitNutriAguaProvider } from '../../providers/easyfit-nutri-agua/easyfit-nutri-agua';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { EasyfitNutriExerciciosProvider } from '../../providers/easyfit-nutri-exercicios/easyfit-nutri-exercicios';

/**
 * Generated class for the HomeTabbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'home-tabbar',
  templateUrl: 'home-tabbar.html'
})
export class HomeTabbarComponent {

  text: string;

  constructor(
    public helpers: HelpersProvider,
    public auth: AuthProvider,
    public _agua: EasyfitNutriAguaProvider,
    public _alimentos: EasyfitNutriAlimentosProvider,
    public _exercicios: EasyfitNutriExerciciosProvider
  ) {
    console.log('Hello HomeTabbarComponent Component');
    this.text = 'Hello World';
  }

  titulo() {
    let semanas = this.helpers.moment().diff(
      this.helpers.moment(this.auth.user.user.created_at, 'YYYY-MM-DD HH:mm:ss'),
      'weeks'
    )

    return (semanas + 1) + 'ª semana - ' + this.helpers.moment().format('[hoje, ] DD [de] MMMM')
  }

  getDay() {
    this._agua.get().subscribe();
    this._alimentos.getDay();
    this._exercicios.getDay();
  }

}
