import { Component, Input, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SearchPage } from '../../pages/search/search';
import { PlayerPage } from '../../pages/player/player';
import { PerfilPage } from '../../pages/perfil/perfil';

/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'header',
  templateUrl: 'header.html'
})
export class HeaderComponent {
  @Input() title;
  @Input() searchPage = false;
  @Input() showMenu = true;
  @Input() showPerfil = false;
  @Input() hideButtons = false;
  @Input() hideSearch = false;
  @Output() searchSubmit = new EventEmitter<any>();
  search = ''
  searchOpen = false;

  @ViewChild('input', { read: ElementRef }) input: ElementRef;


  constructor(
    public navCtrl: NavController
  ) {
  }

  doSearch() {
    if (this.searchPage) {
      this.searchSubmit.next(this.search);
    } else {
      this.navCtrl.push(SearchPage, { search: this.search })
    }
    this.closeSearch();
  }

  closeSearch() {
    this.searchOpen = false;
    setTimeout(() => this.search = '', 500)
  }
  openSearch() {
    this.input.nativeElement.focus();
    this.searchOpen = true;
  }

  toggleSearch() {
    this.searchOpen = !this.searchOpen;
  }

  perfilPage() {
    this.navCtrl.push(PerfilPage, {hideButtons: true})
  }
}
