import { Component } from '@angular/core';
import { EasyfitNutriAlimentosProvider } from '../../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { EasyfitNutriAguaProvider } from '../../providers/easyfit-nutri-agua/easyfit-nutri-agua';
import { EasyfitNutriExerciciosProvider } from '../../providers/easyfit-nutri-exercicios/easyfit-nutri-exercicios';
import { HelpersProvider } from '../../providers/helpers/helpers';

/**
 * Generated class for the RadioWeekComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'radio-week',
  templateUrl: 'radio-week.html'
})
export class RadioWeekComponent {

  constructor(
    public _alimentos: EasyfitNutriAlimentosProvider,
    public _agua: EasyfitNutriAguaProvider,
    public _exercicios: EasyfitNutriExerciciosProvider,
    public helpers: HelpersProvider
  ) {
    
  }

  getDay() {
    this._alimentos.getDay();
    this._agua.get().subscribe();
    this._exercicios.getDay();
  }

  change(day) {
    this._alimentos.selectedDate = day;
    console.log('clicou')
    this.getDay();
  }

}
