import { Component } from '@angular/core';
import { EasyfitNutriAguaProvider } from '../../providers/easyfit-nutri-agua/easyfit-nutri-agua';

/**
 * Generated class for the ConsumoAguaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'consumo-agua',
  templateUrl: 'consumo-agua.html'
})
export class ConsumoAguaComponent {

  text: string;

  constructor(
    public _agua: EasyfitNutriAguaProvider
  ) {
    console.log('Hello ConsumoAguaComponent Component');
    this.text = 'Hello World';
  }

}
