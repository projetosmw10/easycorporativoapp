import { NgModule } from '@angular/core';
import { ImagePreloadDirective } from './image-preload/image-preload';
@NgModule({
	declarations: [ImagePreloadDirective,
    ImagePreloadDirective],
	imports: [],
	exports: [ImagePreloadDirective,
    ImagePreloadDirective]
})
export class DirectivesModule {}
