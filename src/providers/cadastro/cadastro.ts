import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CadastroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CadastroProvider {

  dados: any = {
    genero: '',
    data_nascimento: '',
    altura: '',
    inicio_kg: '',
    objetivo_descricao: '',
    objetivo_kg: '',
    objetivo_kcal: '',
    first_name: '',
    telefone: '',
    email: '',
    password: '',
    confirmPassword: ''
  }

  constructor(
    public http: HttpClient
  ) {
    console.log('Hello CadastroProvider Provider');
  }

  clear() {
    this.dados = {
      genero: '',
      data_nascimento: '',
      altura: '',
      inicio_kg: '',
      objetivo_descricao: '',
      objetivo_kg: '',
      objetivo_kcal: '',
      first_name: '',
      telefone: '',
      email: '',
      password: '',
      confirmPassword: ''
    }
  }

}
