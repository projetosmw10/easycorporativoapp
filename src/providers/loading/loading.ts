import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';

/*
  Generated class for the LoadingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoadingProvider {
  loading: Loading;
  constructor(
    public loadingCtrl: LoadingController
  ) {
    console.log('Hello LoadingProvider Provider');
  }

  present() {
    // this.loading = this.loadingCtrl.create({
    //   spinner: 'hide',
    //   content: `
    //   <div class="custom-spinner-container">
    //     <img class="loading" width="80" height="80" src="assets/loading.gif" />
    //   </div>`
    // });

    this.loading = this.loadingCtrl.create();
    return this.loading.present();
  }


  dismiss() {
    return new Promise(resolve => {
      if (this.loading) {
        return this.loading.dismiss(resolve(true)).catch(error => {
          console.log('loading error: ', error);
        });
      } else {
        resolve(true);
      }
    });

  }

}
