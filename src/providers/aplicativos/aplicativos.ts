import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { HelpersProvider } from '../helpers/helpers';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategoriasProvider } from '../categorias/categorias';
import { StatusBar } from '@ionic-native/status-bar';
import { App } from 'ionic-angular';

/*
  Generated class for the AplicativosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AplicativosProvider {
  item: any = {};
  list = [];
  constructor(
    public api: ApiProvider,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public app: App
  ) {
    this.api.saveOneSignal();
    this.splashScreen.hide();
    console.log('Hello AplicativosProvider Provider');
  }

  public getAll() {
    return new Promise((resolve, reject) => this.api.get('/aplicativos2').subscribe(r => resolve(this.list = r), e => reject(e)))
  }

  setItem(item) {
    
    this.item = item;
    
    
    this.setTheme(item);
  }
  setTheme(item) {
    this.statusBar.backgroundColorByHexString(item.cor);
    
    
    document.body.setAttribute('class', '');
    if (item.id == 1) {
      document.body.classList.add('theme-easymind')
    } else if (item.id == 2) {
      document.body.classList.add('theme-easyfit-sports')
    } else if (item.id == 3) {
      document.body.classList.add('theme-easyfit-nutri')
    } else if (item.id == 4) {
      document.body.classList.add('theme-easylife')
    }
    else if (item.id == 5) {
      document.body.classList.add('theme-easylife')
      document.body.classList.add('theme-easyfit-gestao')
    }
    else if (item.id == 6) {
      document.body.classList.add('theme-easyfit-nutri')
      document.body.classList.add('theme-easyfit-central')
    }
    else if (item.id == 7) {
      console.log('setou 7')
      document.body.classList.add('theme-easylife-gastronomia')
    }

  }
  clearItem() {
    this.item = {};
  }

  getEasyfitNutri() {
    return this.list.find(obj => obj.id == 3)
  }
  getEasyfitLife() {
    return this.list.find(obj => obj.id == 4)
  }


}
