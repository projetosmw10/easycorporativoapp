import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { ToastController, AlertController } from 'ionic-angular';

/*
  Generated class for the ConteudosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConteudosProvider {
  list: any = [];
  
  constructor(
    public api: ApiProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController
  ) {
  }

  get(url?) {
    if (!url) {
      url = '/conteudos';
    }
    return this.api.get(url)
  }

  getAll(options: any = {}) {
    let params = new URLSearchParams();
    for (let key in options) {
      params.set(key, options[key]);
    }

    let queryStr = params.toString() ? '?' + params.toString() : '';
    return this.get('/conteudos' + queryStr).map(r => this.list = r)
  }

  descricao(item) {
    this.alertCtrl.create({
      title: item.titulo, message: item.descricao.replace(/(?:\r\n|\r|\n)/g, '<br>'), cssClass: 'alert-lg', buttons: [{
        role: 'cancel',
        text: 'Ok'
      }]
    }).present();
  }

  visualizar(conteudo_id) {
    this.list = this.list.map(conteudo => {
      if (conteudo.id == conteudo_id) {
        conteudo.assistido = 1;
      }
      return conteudo
    })
    return this.api.patch('/conteudos/' + conteudo_id + '/visualizar')
  }


  favoritar(conteudo) {
    console.log(`favoritar`, conteudo)
    return new Promise(resolve => {
      if (conteudo.favoritado != 1) {
        this.patchFavoritar(conteudo.id).subscribe(() => {
          this.toastCtrl.create({ message: conteudo.titulo + ' adicionado aos seus favoritos ', duration: 3000 }).present();
          resolve();
        })

      } else {
        this.deleteFavoritar(conteudo.id).subscribe(() => {
          this.toastCtrl.create({ message: conteudo.titulo + ' removido de seus favoritos ', duration: 3000 }).present();
          resolve();
        });
      }
    })
  }
  
  patchFavoritar(conteudo_id) {
    this.list = this.list.map(conteudo => {
      if (conteudo.id == conteudo_id) {
        conteudo.favoritado = 1;
      }
      return conteudo
    })
    return this.api.patch('/conteudos/' + conteudo_id + '/favoritar')
  }


  deleteFavoritar(conteudo_id) {
    this.list = this.list.map(conteudo => {
      if (conteudo.id == conteudo_id) {
        conteudo.favoritado = 0;
      }
      return conteudo
    })
    return this.api.delete('/conteudos/' + conteudo_id + '/favoritar')
  }
}
