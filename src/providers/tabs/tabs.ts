import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { App } from 'ionic-angular';
import { EasymindTabsPage } from '../../pages/easymind-tabs/easymind-tabs';
import { AplicativosProvider } from '../aplicativos/aplicativos';
import { EasyfitSportsTabsPage } from '../../pages/easyfit-sports-tabs/easyfit-sports-tabs';

/*
  Generated class for the TabsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TabsProvider {

  _selectTab = new Subject<number>();

  constructor(
    public app: App,
    public aplicativos: AplicativosProvider
  ) {
    console.log('Hello TabsProvider Provider');
  }

  selectTab(index: number) {

    console.log('open tab 3')
    if (this.aplicativos.item.id == 1) {
      const tabPage = this.app.getRootNav().getViews().find(v => v.component.name == 'EasymindTabsPage');
      if (tabPage) {
        // console.log(tabPage, 'FOUND');
        this._selectTab.next(index);
      } else {
        this.app.getRootNav().setRoot(EasymindTabsPage, { select: index }, { animate: true, animation: 'md-transition', duration: 1000, direction: 'forward' });
      }
    }
    // this.app.getRootNav().setRoot(TabsPage, { select: 3 }, { animate: true, animation: 'wp-transition' });
  }

}
