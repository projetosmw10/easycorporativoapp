import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CodigoPage } from '../../pages/codigo/codigo';
import { InAppBrowserOptions, InAppBrowser } from '@ionic-native/in-app-browser';
import { PlayerPage } from '../../pages/player/player';
import { App, AlertController, Platform } from 'ionic-angular';
import { AuthProvider } from '../auth/auth';
import { GlobalProvider } from '../global/global';
import { CategoriasProvider } from '../categorias/categorias';
import { AplicativosProvider } from '../aplicativos/aplicativos';
import { HelpersProvider } from '../helpers/helpers';

/*
  Generated class for the PlayerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlayerProvider {
  constructor(
    public http: HttpClient,
    public app: App,
    public alert: AlertController,
    public auth: AuthProvider,
    public global: GlobalProvider,
    public iap: InAppBrowser,
    public plt: Platform,
    public _categorias: CategoriasProvider,
    public aplicativos: AplicativosProvider,
    public helpers: HelpersProvider
  ) {
    console.log('Hello PlayerProvider Provider');

  }

  getNavCtrl() {
    return this.app.getActiveNavs()[0];
  }
  playerPage(item) {


    if (this.auth.isPremium()) {
      this.openPlayerPage(item);
    } else {
      if (!(item.gratuito) && !this.helpers.isAppleReview()) {
        this.alert.create({
          title: 'Oops',
          message: this.global.informacoes.codigo_necessario,
          buttons: [
            {
              role: 'cancel',
              text: 'Cancelar'
            },
            {
              text: 'Ok',
              handler: () => {
                this.getNavCtrl().push(CodigoPage);
              }
            }
          ]
        }).present();
      } else {
        this.openPlayerPage(item);
      }
    }
  }


  openPlayerPage(item) {
    if (item.tipo == 'pdf') {
      if (!item.thumbnail_principal) {
        this.alert.create({ title: 'Oops', message: 'Não foi possível carregar, por favor tente novamente mais tarde' }).present();
        return;
      }

      let options: InAppBrowserOptions = {
        hideurlbar: 'yes',
        hidenavigationbuttons: 'yes',
        fullscreen: 'yes'
      }

      if (this.plt.is('ios')) {
        if (this.aplicativos.item.id == 1) {
          this.iap.create(item.thumbnail_principal, '_blank', options).show();
        } else {
          window.open(item.thumbnail_principal, '_system');
        }
      } else {
        this.iap.create(item.thumbnail_principal, '_blank', options).show();
        // window.open(item.thumbnail_principal, '_system');
      }

      this._categorias.visualizar(item.id).subscribe(e => {
        item.assistido = 1;
      });
    } else {
      this.getNavCtrl().push(PlayerPage, { item: item });
    }
  }
}
