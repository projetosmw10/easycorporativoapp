import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, AlertController, ToastController, Platform } from 'ionic-angular';
import { ApiProvider } from '../api/api';
import { FileUploadOptions } from '@ionic-native/file-transfer';

/*
  Generated class for the UploadsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UploadsProvider {
  multiInfo = {
    files: new Array<string>(),
    toSend: 0,
    sent: 0,
    failed: new Array<string>(),
    loadingStr: '',
    url: '',
    loading: this.loadingCtrl.create({ content: '' }),
    response: []
  }
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public api: ApiProvider,
    public plt: Platform,
  ) {
    console.log('Hello UploadsProvider Provider');
  }
  multipleUpload(file, options?: FileUploadOptions) {

    return new Promise(
      (resolve) => {
        this.multiInfo.loading.present();
        this.multiInfo.loadingStr = 'Enviando ' + (this.multiInfo.sent + 1) + '/' + this.multiInfo.toSend + ' arquivos';
        this.api.uploadProgress.subscribe(r => {
          let porcentagem = ' ' + r + '%';
          this.multiInfo.loading.setContent(this.multiInfo.loadingStr);
          /**
           * @todo arrumar porcentagem, nao está calculando corretamente eu acredito
           */
          // this.multiInfo.loading.setContent(this.multiInfo.loadingStr + ' - ' + porcentagem);
        })
        this.api.uploadFiles(file, this.multiInfo.url).then(r => {
          this.multiInfo.response.push(r);
          console.log('multipleUpload => r', r)
          this.multiInfo.sent++;
          if (this.multiInfo.sent < this.multiInfo.toSend) {
            this.multipleUpload(this.multiInfo.files[this.multiInfo.sent], options);
          } else {
            resolve(this.multiInfo.response)
          }
        }).catch(e => {
          console.error('uploadMultiple => catch ', e)
          this.multiInfo.failed.push(this.multiInfo.files[this.multiInfo.sent]);

          this.multiInfo.sent++;
          if (this.multiInfo.sent < this.multiInfo.toSend) {
            this.multipleUpload(this.multiInfo.files[this.multiInfo.sent], options);
          } else {
            resolve(this.multiInfo.response)
          }
        })
      }
    )
  }
  startMultipleUpload(array, url, options?: FileUploadOptions) {
    if (!Array.isArray(array)) {
      array = [array];
    }
    return new Promise(
      (resolve, reject) => {
        this.multiInfo.url = url;
        this.multiInfo.files = array.filter(obj => obj);
        this.multiInfo.toSend = array.length;
        if (this.multiInfo.files[0]) {
          this.multipleUpload(this.multiInfo.files[0], options).then(
            r => {
              resolve(this.multiInfo.response);
              this.onEndMultipleUpload();
            }
          );
        } else {
          reject('Nenhum arquivo foi informado')
        }
      }
    )
  }

  onEndMultipleUpload() {
    if (this.multiInfo.failed.length) {
      const strFailed = this.multiInfo.failed.map(v => v.split('/').pop() + "\n");
      const alert = this.alertCtrl.create({
        title: 'Erro ao enviar arquivos',
        message: 'Não foi possível enviar ' + this.multiInfo.failed.length + ' arquivos: \n' + strFailed
      })
      alert.present();
    }
    if (this.multiInfo.failed.length !== this.multiInfo.files.length) {
      const toast = this.toastCtrl.create({ message: (this.multiInfo.files.length - this.multiInfo.failed.length) + ' arquivos enviados', duration: 3000 })
      toast.present()
    }
    this.resetMultiInfo();
  }

  resetMultiInfo() {
    this.multiInfo.loading.dismiss();
    this.multiInfo = {
      files: new Array<string>(),
      toSend: 0,
      sent: 0,
      failed: new Array<string>(),
      loadingStr: '',
      loading: this.loadingCtrl.create({ content: '' }),
      url: '',
      response: []
    }
  }

}
