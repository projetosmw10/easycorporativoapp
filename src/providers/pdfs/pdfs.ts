import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InAppBrowserOptions, InAppBrowser } from '@ionic-native/in-app-browser';
import { ApiProvider } from '../api/api';
import { AlertController, ToastController, Platform } from 'ionic-angular';
import { AplicativosProvider } from '../aplicativos/aplicativos';

/*
  Generated class for the PdfsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PdfsProvider {

  list: any = []

  constructor(
    public api: ApiProvider,
    public iap: InAppBrowser,
    public plt: Platform,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public _aplicativos: AplicativosProvider
  ) {
    console.log('Hello PdfsProvider Provider');
  }

  getAll() {
    return this.api.get('/pdfs?aplicativo_id='+this._aplicativos.item.id).map(r => this.list = r);
  }
  getAllIfNecessary() {
    // if(this.list.length == 0) {
      this.getAll().subscribe();
    // }
  }

  open(pdf) {
    if (!pdf.thumbnail_principal) {
      this.alertCtrl.create({ title: 'Oops', message: 'Não foi possível carregar, por favor tente novamente mais tarde' }).present();
      return;
    }

    let options: InAppBrowserOptions = {
      hideurlbar: 'yes',
      hidenavigationbuttons: 'yes',
      fullscreen: 'yes'
    }

    if (this.plt.is('ios')) {
      this.iap.create(pdf.thumbnail_principal, '_blank', options).show();
    } else {
      // this.iap.create(pdf.thumbnail_principal, '_system', options).show();
      window.open(pdf.thumbnail_principal, '_system');
    }

    this.ler(pdf.id).subscribe();
  }


  ler(pdf_id) {
    this.list = this.list.map(pdf => {
      if (pdf.id == pdf_id) {
        pdf.lido = 1;
      }
      return pdf
    })
    return this.api.patch('/pdfs/' + pdf_id + '/ler')
  }


  favoritar(pdf) {
    console.log(`favoritar`, pdf)
    return new Promise(resolve => {
      if (pdf.favoritado != 1) {
        this.patchFavoritar(pdf.id).subscribe(() => {
          this.toastCtrl.create({ message: pdf.titulo + ' adicionado aos seus favoritos ', duration: 3000 }).present();
          resolve()
        }, e => resolve());
      } else {
        this.deleteFavoritar(pdf.id).subscribe(() => {
          this.toastCtrl.create({ message: pdf.titulo + ' removido de seus favoritos ', duration: 3000 }).present();
          resolve()
        }, e => resolve());
      }
    })
  }
  patchFavoritar(pdf_id) {
    this.list = this.list.map(pdf => {
      if (pdf.id == pdf_id) {
        pdf.favoritado = 1;
      }
      return pdf
    })
    return this.api.patch('/pdfs/' + pdf_id + '/favoritar')
  }


  deleteFavoritar(pdf_id) {
    this.list = this.list.map(conteudo => {
      if (conteudo.id == pdf_id) {
        conteudo.favoritado = 0;
      }
      return conteudo
    })
    return this.api.delete('/pdfs/' + pdf_id + '/favoritar')
  }

}
