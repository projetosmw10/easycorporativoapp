import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { ToastController } from 'ionic-angular';
import { Subject } from 'rxjs/Subject';
import { AplicativosProvider } from '../aplicativos/aplicativos';
import { Subscription } from 'rxjs/Subscription';

/*
  Generated class for the CategoriasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AreaProvider {
    list: any = [];
    
  watchToggle = new Subject<number>();
  current: any = {}
  _getAll: Subscription;
  constructor(
    public api: ApiProvider,
    public toastCtrl: ToastController,
    public aplicativos: AplicativosProvider
  ) {
    console.log('Hello AreaProvider Provider');
  }

  get(url?) {
    if (!url) {
      url = '/area';
    }
    return this.api.get(url)
  }

  getAll(options: any = {}) {
    const opened = this.list.find(area => area.open);
    return new Promise((resolve, reject) => {
      if (this._getAll && !this._getAll.closed) {
        this._getAll.unsubscribe();
      }
      this._getAll = this.get('/area').map(r => {
        this.list = r;

        return this.list
      }).subscribe(r => resolve(r), e => reject(e))
    })
  }
}
