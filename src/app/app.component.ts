import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, Nav, AlertController, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { CategoriasPage } from '../pages/categorias/categorias';
import { AudiosPage } from '../pages/audios/audios';
import { FraseDoDiaPage } from '../pages/frase-do-dia/frase-do-dia';
import { FavoritosPage } from '../pages/favoritos/favoritos';
import { PerfilPage } from '../pages/perfil/perfil';
import { HelpersProvider } from '../providers/helpers/helpers';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../providers/auth/auth';
import { LoadingProvider } from '../providers/loading/loading';
import { GlobalProvider } from '../providers/global/global';
import { OneSignal } from '@ionic-native/onesignal';
import { ApiProvider } from '../providers/api/api';
import { AplicativosPage } from '../pages/aplicativos/aplicativos';
import { LoginCodigoPage } from '../pages/login-codigo/login-codigo';
import { TabsProvider } from '../providers/tabs/tabs';
import { AplicativosProvider } from '../providers/aplicativos/aplicativos';
import { ContatoPage } from '../pages/contato/contato';
import { ConhecaPage } from '../pages/conheca/conheca';
import { PdfsProvider } from '../providers/pdfs/pdfs';
import { CategoriasProvider } from '../providers/categorias/categorias';
import { EasyfitSportsTabsPage } from '../pages/easyfit-sports-tabs/easyfit-sports-tabs';
import { EasyfitNutriTabsPage } from '../pages/easyfit-nutri-tabs/easyfit-nutri-tabs';
import { PdfsEDicasPage } from '../pages/pdfs-e-dicas/pdfs-e-dicas';
import { CategoriasConteudosPage } from '../pages/categorias-conteudos/categorias-conteudos';
import { CertificadosPagarPage } from '../pages/certificados-pagar/certificados-pagar';
// import { LoginPage } from '../pages/login-2/login-2';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  counter = 0;
  rootPage: any = LoginPage;
  pages = {
    perfil: PerfilPage,
    login: LoginPage,
  }

  constructor(
    public global: GlobalProvider,
    public plt: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public tabs: TabsProvider,
    public auth: AuthProvider,
    public loading: LoadingProvider,
    public oneSignal: OneSignal,
    public api: ApiProvider,
    public alertCtrl: AlertController,
    public aplicativos: AplicativosProvider,
    public _pdfs: PdfsProvider,
    public _categorias: CategoriasProvider,
    public app: App,
    public helpers: HelpersProvider
  ) {
    /***
     * ATENCAO SPLASHSCREEN HIDE NO TABSPAGE E LOGIN PAGE
     * ATENCAO SPLASHSCREEN HIDE NO TABSPAGE E LOGIN PAGE
     * ATENCAO SPLASHSCREEN HIDE NO TABSPAGE E LOGIN PAGE
     * ATENCAO SPLASHSCREEN HIDE NO TABSPAGE E LOGIN PAGE
     */

    this.global.loadingApp = false;
    this.auth.setToken(JSON.parse(window.localStorage.getItem("token")));
    if (this.auth.isLoggedIn()) {
      if (this.auth.isPremium()) {
        this.rootPage = AplicativosPage;
      } else {
        this.rootPage = LoginCodigoPage;
      }
    } else {
      this.rootPage = LoginPage;
    }
    this.initializeApp();

  }

  initializeApp() {

    this.plt.ready().then(() => {

      // Okay, so the plt is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      if (this.plt.is('cordova')) {
        this.oneSignal.startInit('2a60418c-204b-47b4-9a70-e0053a92ed16', '250982028144');

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

        this.oneSignal.handleNotificationOpened().subscribe((r) => {
          const data = r.notification.payload.additionalData;
          // if (data.table == 'codes') {
          //   this.openTabPage(6);
          // }
          // do something when a notification is opened
          // console.log(r);
          this.alertCtrl.create({ title: r.notification.payload.title, message: r.notification.payload.body }).present();
        });

        this.oneSignal.getIds().then(r => {
          this.auth.player_id = r.userId;
          this.api.saveOneSignal();
        });

        this.oneSignal.endInit();


      }

      this.statusBar.backgroundColorByHexString('#000000');
      if (this.plt.is('android')) {
        this.statusBar.styleLightContent();
      }
      if (this.plt.is('ios')) {
        this.statusBar.styleDefault();
      }


      this.plt.registerBackButtonAction(() => {
        const nav = this.app.getActiveNav();

        if (nav.canGoBack()) {
          nav.pop();
        } else if (this.counter == 0) {
          this.counter++;
          this.helpers.toast('Pressione novamente para sair.')
          setTimeout(() => { this.counter = 0 }, 3000)
        } else {
          this.plt.exitApp();
        }
      }, 0);

    });
  }

  openPage(page) {
    this.nav.setRoot(page, {}, { animate: true, duration: 1000, animation: 'wp-transition', direction: 'forward' })
  }

  openTabPage(index) {
    this.tabs.selectTab(index);
  }

  easyfitSportsTab(index) {
    this.nav.setRoot(EasyfitSportsTabsPage, { select: index })
  }

  easyfitSportsTabsPage() {
    this.nav.setRoot(EasyfitSportsTabsPage);
  }
  favoritosPage() {
    this.nav.push(FavoritosPage);
  }
  categoriasCounteudosPage() {
    this.nav.push(CategoriasConteudosPage);
  }
  pdfsEDicasPage() {
    this.nav.push(PdfsEDicasPage)
  }

  easyfitNutriTab(index) {
    this.nav.setRoot(EasyfitNutriTabsPage, { select: index })
  }
  easyfitNutriTabsPage() {
    this.nav.setRoot(EasyfitNutriTabsPage)
  }

  aplicativosPage() {
    this.aplicativos.clearItem();
    this.openPage(AplicativosPage);
  }
  contatoPage() {
    this.nav.push(ContatoPage);
  }
  perfilPage() {
    this.nav.push(PerfilPage, { hideButtons: true })
  }
  conhecaPage() {
    this.nav.push(ConhecaPage, { hideButtons: true })
  }
  easylifePage() {
    this.loading.present();
    this.aplicativos.setItem(this.aplicativos.getEasyfitLife())
    setTimeout(() => {
      this.nav.setRoot(EasyfitSportsTabsPage);
    }, 50)
  }
  logout() {

    this.nav.setRoot(LoginPage, {}, { animate: true, duration: 1000, animation: 'wp-transition', direction: 'forward' }).then(() => {
      this.aplicativos.clearItem();
      this._categorias.list = [];
      this._pdfs.list = [];
      this.auth.logout()
    });
  }
}
