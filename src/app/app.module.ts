import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { FileTransfer } from '@ionic-native/file-transfer'
import { ImagePicker } from '@ionic-native/image-picker'
import { Camera } from '@ionic-native/camera'
import { OneSignal } from '@ionic-native/onesignal'
import { BrMaskerModule } from 'brmasker-ionic-3';

import { MyApp } from './app.component';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';
registerLocaleData(ptBr)

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { HeaderComponent } from '../components/header/header';
import { CategoriasPage } from '../pages/categorias/categorias';
import { FraseDoDiaPage } from '../pages/frase-do-dia/frase-do-dia';
import { FavoritosPage } from '../pages/favoritos/favoritos';
import { AudiosPage } from '../pages/audios/audios';
import { PerfilPage } from '../pages/perfil/perfil';
import { HelpersProvider } from '../providers/helpers/helpers';
import { HttpClientModule } from '@angular/common/http';
import { DirectivesModule } from '../directives/directives.module';
import { AuthProvider } from '../providers/auth/auth';
import { GlobalProvider } from '../providers/global/global';
import { ApiProvider } from '../providers/api/api';
import { UploadsProvider } from '../providers/uploads/uploads';
import { LoadingProvider } from '../providers/loading/loading';
import { FacaPartePage } from '../pages/faca-parte/faca-parte';
import { IonicStorageModule } from '@ionic/storage';
import { PlayerPage } from '../pages/player/player';
import { CodigoPage } from '../pages/codigo/codigo';
import { FileChooser } from '@ionic-native/file-chooser';
import { SearchPage } from '../pages/search/search';
import { EsqueciSenhaPage } from '../pages/esqueci-senha/esqueci-senha';
import { SublistItemComponent } from '../components/sublist-item/sublist-item';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ConhecaPage } from '../pages/conheca/conheca';
import { SafePipe } from '../pipes/safe/safe';
import { LinkifyPipe } from '../pipes/linkify/linkify';
import {NgxMaskModule} from 'ngx-mask'
import { LoginCodigoPage } from '../pages/login-codigo/login-codigo';
import { AplicativosPage } from '../pages/aplicativos/aplicativos';
import { AplicativosProvider } from '../providers/aplicativos/aplicativos';
import { EasymindTabsPage } from '../pages/easymind-tabs/easymind-tabs';
import { TabsProvider } from '../providers/tabs/tabs';
import { CategoriasListaPage } from '../pages/categorias-lista/categorias-lista';
import { CategoriasProvider } from '../providers/categorias/categorias';
import { SublistItemDesktopComponent } from '../components/sublist-item-desktop/sublist-item-desktop';
import { PlayerProvider } from '../providers/player/player';
import { ContatoPage } from '../pages/contato/contato';
import { PdfsEDicasPage } from '../pages/pdfs-e-dicas/pdfs-e-dicas';
import { PdfsProvider } from '../providers/pdfs/pdfs';
import { ConteudosProvider } from '../providers/conteudos/conteudos';
import { EasyfitSportsHomePage } from '../pages/easyfit-sports-home/easyfit-sports-home';
import { EasyfitSportsTabsPage } from '../pages/easyfit-sports-tabs/easyfit-sports-tabs';
import { TabsComponent } from '../components/tabs/tabs';
import { CategoriasConteudosPage } from '../pages/categorias-conteudos/categorias-conteudos';
import { CadastroUsuarioPage } from '../pages/cadastro-usuario/cadastro-usuario';
import { CadastroUsuarioSecondPage } from '../pages/cadastro-usuario-second/cadastro-usuario-second';
import { CadastroProvider } from '../providers/cadastro/cadastro';
import { EasyfitNutriAlimentosProvider } from '../providers/easyfit-nutri-alimentos/easyfit-nutri-alimentos';
import { EasyfitNutriAguaProvider } from '../providers/easyfit-nutri-agua/easyfit-nutri-agua';
import { EasyfitNutriExerciciosProvider } from '../providers/easyfit-nutri-exercicios/easyfit-nutri-exercicios';
import { EasyfitNutriTabsPage } from '../pages/easyfit-nutri-tabs/easyfit-nutri-tabs';
import { EasyfitNutriCalculoCaloricoPage } from '../pages/easyfit-nutri-calculo-calorico/easyfit-nutri-calculo-calorico';
import { EasyfitNutriControleAguaPage } from '../pages/easyfit-nutri-controle-agua/easyfit-nutri-controle-agua';
import { EasyfitNutriExercicioInternaPage } from '../pages/easyfit-nutri-exercicio-interna/easyfit-nutri-exercicio-interna';
import { EasyfitNutriExerciciosPage } from '../pages/easyfit-nutri-exercicios/easyfit-nutri-exercicios';
import { EasyfitNutriHomePage } from '../pages/easyfit-nutri-home/easyfit-nutri-home';
import { EasyfitNutriRefeicoesInternaPage } from '../pages/easyfit-nutri-refeicoes-interna/easyfit-nutri-refeicoes-interna';
import { EasyfitNutriRefeicoesPage } from '../pages/easyfit-nutri-refeicoes/easyfit-nutri-refeicoes';
import { ToFixedPipe } from '../pipes/to-fixed/to-fixed';
import { FilterPipe } from '../pipes/filter/filter';
import { ConsumoAguaComponent } from '../components/consumo-agua/consumo-agua';
import { HomeTabbarComponent } from '../components/home-tabbar/home-tabbar';
import { RadioWeekComponent } from '../components/radio-week/radio-week';
import { GestaoHomePage } from '../pages/gestao-home/gestao-home';
import { GestaoTabsPage } from '../pages/gestao-tabs/gestao-tabs';
import { GestaoCadastroPage } from '../pages/gestao-cadastro/gestao-cadastro';
import { GestaoListagemPage } from '../pages/gestao-listagem/gestao-listagem';
import { GestaoImagensPage } from '../pages/gestao-imagens/gestao-imagens';
import { CentralHomePage } from '../pages/central-home/central-home';
import { CentralTabsPage } from '../pages/central-tabs/central-tabs';
import { AreaProvider } from '../providers/area/area';
import { ConsultasProvider } from '../providers/consultas/consultas';
import { NotificacoesProvider } from '../providers/notificacoes/notificacoes';
import { CentralNotificacoesPage } from '../pages/central-notificacoes/central-notificacoes';
import { NoticiasProvider } from '../providers/noticias/noticias';
import { CentralNoticiasPage } from '../pages/central-noticias/central-noticias';
import { CentralNoticiasInternaPage } from '../pages/central-noticias-interna/central-noticias-interna';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CertificadosPagarPage } from '../pages/certificados-pagar/certificados-pagar';

@NgModule({
  declarations: [
    HeaderComponent,
    SublistItemComponent,
    SublistItemDesktopComponent,
    ConsumoAguaComponent,
    HomeTabbarComponent,
    RadioWeekComponent,
    SafePipe,
    LinkifyPipe,
    ToFixedPipe,
    FilterPipe,
    MyApp,
    AboutPage,
    ContactPage,
    CategoriasPage,
    EasymindTabsPage,
    LoginPage,
    CategoriasListaPage,
    FraseDoDiaPage,
    FavoritosPage,
    AudiosPage,
    PerfilPage,
    FacaPartePage,
    PlayerPage,
    CodigoPage,
    SearchPage,
    EsqueciSenhaPage,
    ConhecaPage,
    LoginCodigoPage,
    AplicativosPage,
    ContatoPage,
    PdfsEDicasPage,
    EasyfitSportsHomePage,
    EasyfitSportsTabsPage,
    TabsComponent,
    CategoriasConteudosPage,
    CadastroUsuarioPage,
    CadastroUsuarioSecondPage,
    EasyfitNutriTabsPage,
    EasyfitNutriCalculoCaloricoPage,
    EasyfitNutriCalculoCaloricoPage,
    EasyfitNutriControleAguaPage,
    EasyfitNutriExercicioInternaPage,
    EasyfitNutriExerciciosPage,
    EasyfitNutriHomePage,
    EasyfitNutriRefeicoesInternaPage,
    EasyfitNutriRefeicoesPage,
    GestaoHomePage,
    GestaoTabsPage,
    GestaoCadastroPage,
    GestaoListagemPage,
    GestaoImagensPage,
    CentralTabsPage,
    CentralHomePage,
    CentralNotificacoesPage,
    CentralNoticiasPage,
    CentralNoticiasInternaPage,
    CertificadosPagarPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      backButtonIcon: 'ios-arrow-back-outline',
      tabsPlacement: 'bottom',
      mode: 'md',
      pageTransition: 'ios-transition',
    }),
    IonicStorageModule.forRoot(),
    DirectivesModule,
    BrMaskerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    CategoriasPage,
    EasymindTabsPage,
    LoginPage,
    CategoriasListaPage,
    FraseDoDiaPage,
    FavoritosPage,
    AudiosPage,
    PerfilPage,
    FacaPartePage,
    PlayerPage,
    CodigoPage,
    SearchPage,
    EsqueciSenhaPage,
    ConhecaPage,
    LoginCodigoPage,
    AplicativosPage,
    ContatoPage,
    PdfsEDicasPage,
    EasyfitSportsHomePage,
    EasyfitSportsTabsPage,
    TabsComponent,
    CategoriasConteudosPage,
    CadastroUsuarioPage,
    CadastroUsuarioSecondPage,
    EasyfitNutriTabsPage,
    EasyfitNutriCalculoCaloricoPage,
    EasyfitNutriCalculoCaloricoPage,
    EasyfitNutriControleAguaPage,
    EasyfitNutriExercicioInternaPage,
    EasyfitNutriExerciciosPage,
    EasyfitNutriHomePage,
    EasyfitNutriRefeicoesInternaPage,
    EasyfitNutriRefeicoesPage,
    GestaoHomePage,
    GestaoTabsPage,
    GestaoCadastroPage,
    GestaoListagemPage,
    GestaoImagensPage,
    CentralTabsPage,
    CentralHomePage,    
    CentralNotificacoesPage,
    CentralNoticiasPage,
    CentralNoticiasInternaPage,
    CertificadosPagarPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativePageTransitions,
    InAppBrowser,
    Camera,
    FileTransfer,
    FileChooser,
    ImagePicker,
    OneSignal,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: 'pt'},
    HelpersProvider,
    CategoriasProvider,
    AuthProvider,
    GlobalProvider,
    ApiProvider,
    UploadsProvider,
    LoadingProvider,
    DocumentViewer,
    AplicativosProvider,
    TabsProvider,
    PlayerProvider,
    PdfsProvider,
    ConteudosProvider,
    TabsProvider,
    CadastroProvider,
    EasyfitNutriAlimentosProvider,
    EasyfitNutriAguaProvider,
    EasyfitNutriExerciciosProvider,
    AreaProvider,
    ConsultasProvider,
    NotificacoesProvider,
    NoticiasProvider,
    SocialSharing    
  ]
})
export class AppModule { }
